<?php

namespace SafePay\Blockchain;

defined('SPSEC') || exit;

interface DBRecipientCMS
{
    /**
     * Получение реципиента по аттрибуту
     *
     * @param string $attr
     *
     * @return object|bool
     */
    public static function getByAttribute($attr);

    /**
     * Получение всех реципиентов
     * @return object|bool
     */
    public static function getListAll();

}