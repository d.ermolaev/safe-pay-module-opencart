<?php

namespace SafePay\Blockchain;

use PDO;

defined('SPSEC') || exit;

class OptionsOpencart implements OptionsCMS
{

    /**
     * OptionsWordpress constructor.
     */
    public static function getPublicKey()
    {
        return self::getAttribute('payment_safepay_public_key');
    }

    /**
     * Получение публичного ключа
     *
     * @return string
     */
    public static function getPrivateKey()
    {
        return self::getAttribute('payment_safepay_private_key');
    }

    /**
     * Получение счёта
     *
     * @return string
     */
    public static function getAddress()
    {
        return self::getAttribute('payment_safepay_bild');
    }

    /**
     * Получение срока жизни счета
     *
     * @return int
     */
    public static function getExpire()
    {
        if (!empty(self::getAttribute('payment_safepay_interval_pay_expired'))) {
            return self::getAttribute('payment_safepay_interval_pay_expired');
        } else {
            return 12;
        }
    }

    /**
     * Получение режима работы платежной системы
     *
     * @return int
     */
    public static function isTest()
    {
        if (self::getAttribute('payment_safepay_test') == 'on') {
            return 1;
        } else {
            return 0;
        }
    }

    /**
     * Получение callback ссылки для банка, об успешной транзакции
     *
     * @return string
     */
    public static function getCallback()
    {
        return HTTPS_SERVER . 'index.php?route=extension/payment/safepay/callback';
    }

    /**
     * Обновляет в настройках плагина последний проверенный блок
     *
     * @param int $block
     *
     * @return bool
     */
    public static function setLastBlock($block)
    {
        try {
            $db = DB::getInstance();
            $query = "UPDATE " . DB_PREFIX . "setting SET value = :sp_block WHERE code = 'payment_safepay' AND key = :sp_type";
            $sth = $db->prepare($query);

            return $sth->execute(array(':sp_block' => $block, ':sp_type' => self::getTypeLastBlock()));
        } catch (Exception $e) {
            print $e->getMessage();
            return false;
        }
    }

    /**
     * Получает последний блок из настроек плагина
     *
     * @return int
     */
    public static function getLastBlock()
    {
        if (!empty(self::getAttribute(self::getTypeLastBlock()))) {
            return self::getAttribute(self::getTypeLastBlock());
        } else {
            return 0;
        }
    }

    /**
     * Получает интервал для проверки оплаты через Cron
     *
     * @return int
     */
    public static function getPayInterval()
    {
        if (!empty(self::getAttribute('payment_safepay_interval_pay'))) {
            return self::getAttribute('payment_safepay_interval_pay');
        } else {
            return 5;
        }
    }

    /**
     * Получает интервал для проверки серверов через Cron
     *
     * @return int
     */
    public static function getServerInterval()
    {
        if (!empty(self::getAttribute('payment_safepay_interval_server'))) {
            return self::getAttribute('payment_safepay_interval_server');
        } else {
            return 20;
        }
    }

    /**
     * Обновляем статус в магазине на "Оплачено"
     *
     * @param int $order_id
     *
     * @return bool
     */
    public static function completedPay($order_id)
    {
        if (self::update_order_status($order_id, 2)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Получает шаблон для заголовка транзакции
     *
     * @param array $param
     *
     * @return string
     */
    public static function getTemplateTitle($param)
    {
        if (!empty(self::getAttribute('payment_safepay_temp_title'))) {
            $text = self::getAttribute('payment_safepay_temp_title');
        } else {
            $text = 'Оплата заказа из интернет-магазина %site_url%.';
        }

        return self::getCorrectTemplate($text, $param);
    }

    /**
     * Получает шаблон для описания транзакции
     *
     * @param array $param
     *
     * @return string
     */
    public static function getTemplateDescription($param)
    {
        if (!empty(self::getAttribute('payment_safepay_temp_desc'))) {
            $text = self::getAttribute('payment_safepay_temp_desc');
        } else {
            $text = 'Оплата по счету №%order_id% от %order_date% на сумму %order_sum%.';
        }

        return self::getCorrectTemplate($text, $param);
    }

    /**
     * Подстановка параметров в текст
     *
     * @param string $text
     * @param array $param
     *
     * @return string
     */
    private static function getCorrectTemplate($text, $param)
    {
        $text = str_replace('%order_id%', $param['order_id'], $text);
        $text = str_replace('%order_date%', $param['order_date'], $text);
        $text = str_replace('%order_sum%', $param['order_sum'], $text);
        $text = str_replace('%site_url%', $param['site_url'], $text);

        return $text;
    }

    /**
     * Получает название поля последнего блока для тестового или боевого сервера
     *
     * @return string
     */
    private static function getTypeLastBlock()
    {
        if (self::isTest()) {
            return 'payment_safepay_last_t';
        } else {
            return 'payment_safepay_last_l';
        }
    }

    /**
     * Получение параметра настроек плагина
     *
     * @param string $type
     *
     * @return mixed
     */
    private static function getAttribute($type)
    {
        $settings = self::getSettings();
        if (!empty($settings[$type])) {
            return $settings[$type];
        } else {
            return false;
        }
    }

    /**
     * Получение настроек плагина
     *
     * @return array|object|bool
     */
    private static function getSettings()
    {
        try {
            $db = DB::getInstance();
            $sth = $db->query("SELECT `key`, `value` FROM " . DB_PREFIX . "setting WHERE code = 'payment_safepay'");

            if ($settings = $sth->fetchAll(PDO::FETCH_CLASS)) {
                $array = array();
                foreach ($settings as $settings_item) {
                    $array[$settings_item->key] = $settings_item->value;
                }
                return $array;
            } else {
                return false;
            }
        } catch (Exception $e) {
            print $e->getMessage();
            return false;
        }
    }

    /**
     * Обновление статуса заказа
     *
     * @param int $id
     * @param int $status
     * @return bool
     */
    private static function update_order_status($id, $status)
    {
        try {
            $db = DB::getInstance();
            $query = "UPDATE " . DB_PREFIX . "order SET order_status_id = :order_status_id WHERE order_id= :order_id";
            $sth = $db->prepare($query);

            if ($sth->execute(array(':order_status_id' => $status, ':order_id' => $id))) {
                self::update_order_history($id, $status);
                return true;
            } else {
                return false;
            }
        } catch (Exception $e) {
            print $e->getMessage();
            return false;
        }
    }

    /**
     * Добавление нового статуса в историю заказа
     *
     * @param int $id
     * @param int $status
     * @return bool
     */
    private static function update_order_history($id, $status)
    {
        try {
            $db = DB::getInstance();
            $query = "INSERT INTO " . DB_PREFIX . "order_history (order_id, order_status_id) VALUES (:order_id, :order_status_id)";
            $query = "INSERT INTO " . DB_PREFIX . "order_history (order_id, order_status_id, comment, date_added) VALUES (:order_id, :order_status_id, '', :order_update_time)";
            $sth = $db->prepare($query);

            return $sth->execute(
                array(
                    ':order_id'          => $id,
                    ':order_status_id'   => $status,
                    ':order_update_time' => date('Y-m-d H:i:s')
                )
            );
        } catch (Exception $e) {
            print $e->getMessage();
            return false;
        }
    }
}