<?php

namespace SafePay\Blockchain;

defined('SPSEC') || exit;

class Cron
{
    private $logging;

    function __construct()
    {
        $this->logging = Logging::getInstance();
    }

    /**
     * Задача на проверку поступление оплаты
     */
    public function resultPay()
    {
        $process = new Process();
        $process->resultPay();

        $this->logging->log('cron_log', 'Запустилась задача на проверку поступления оплаты');
    }

    /**
     * Задача на проверку серверов по дате последнего блока
     */
    public function availableServers()
    {
        $dbServer = DBServer::getAllServers();
        foreach ($dbServer as $item) {
            $url = $item->URL_SERVER . "/api/lastblock";

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HTTPGET, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_TIMEOUT, 3);
            $response = curl_exec($ch);
            curl_close($ch);
            $result = json_decode($response, true);

            if ($result["timestamp"] > $item->TIME_UPDATE) {
                DBServer::updateServer($item->ID, $result["timestamp"]);
            }
        }

        $this->logging->log('cron_log', 'Запустилась задача на проверку серверов');
    }

}