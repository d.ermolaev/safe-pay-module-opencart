<?php

namespace SafePay\Blockchain;

defined('SPSEC') || exit;

class DBInvoice extends DBInvoiceOpencart
{

    /**
     * Возвращает название статуса
     *
     * @param string $status
     *
     * @return string|bool
     */
    public static function getStatusName($status)
    {
        $array = array(
            'active'   => __('Активна', 'safe-pay'),
            'finish'   => __('Оплачена', 'safe-pay'),
            'waiting'  => __('В ожидании', 'safe-pay'),
            'canceled' => __('Отменена', 'safe-pay'),
            'expired'  => __('Просрочена', 'safe-pay')
        );
        if (!empty($array[$status])) {
            return $array[$status];
        }

        return false;
    }
}