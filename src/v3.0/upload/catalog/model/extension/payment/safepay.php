<?php

class ModelExtensionPaymentSafepay extends Model
{
    public function __construct($registry)
    {
        parent::__construct($registry);
    }

    /**
     * Получение метода оплаты
     *
     * @param $address
     * @return array
     */
    public function getMethod($address)
    {
        $this->load->language('extension/payment/safepay');
        if (!empty($this->config->get('payment_safepay_status')) && !empty($this->config->get('payment_safepay_public_key')) && !empty($this->config->get('payment_safepay_private_key')) && !empty($this->config->get('payment_safepay_private_key'))) {
            $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$this->config->get('payment_safepay_geo') . "' AND country_id = '" . (int)$address['country_id'] . "' AND (zone_id = '" . (int)$address['zone_id'] . "' OR zone_id = '0')");

            if (!$this->config->get('payment_safepay_geo') || $query->num_rows) {
                $status = true;
            } else {
                $status = false;
            }
        } else {
            $status = false;
        }

        $method_data = array();

        if ($status) {
            $method_data = array(
                'id'         => 'safepay',
                'code'       => 'safepay',
                'title'      => $this->language->get('text_title'),
                'terms'      => '',
                'sort_order' => $this->config->get('payment_safepay_sort_order')
            );
        }
        return $method_data;
    }

    /**
     * Обновление значения параметров
     *
     * @param string $param
     * @param string $value
     */
    public function setParamModule($param, $value)
    {
        $this->db->query("UPDATE `" . DB_PREFIX . "setting` SET `value`='" . $value . "' WHERE `code`='payment_safepay' AND `key`='" . $param . "';");
    }
}