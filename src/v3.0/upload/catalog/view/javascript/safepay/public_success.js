(function($) {
  'use strict';
  $(document).ready(function() {

    let invoice_end = get_invoice_end();

    function get_invoice_end() {
      return $('.sp-payment__invoice_end').data('invoice_end');
    }

    function timer() {
      setInterval(function() {
        --invoice_end;
        let result_time = [], h, m;
        result_time.push(correct_end_time(invoice_end % 60));
        m = Math.floor(invoice_end / 60);
        if (m > 60) {
          result_time.push(correct_end_time(m % 60));
          h = Math.floor(m / 60);
          if (h > 24) {
            result_time.push(correct_end_time(h % 24));
            result_time.push(correct_end_time(Math.floor(h / 24)));
          } else {
            result_time.push(correct_end_time(h));
          }
        } else {
          result_time.push(correct_end_time(m));
        }
        result_time.reverse();
        let result_time_view = result_time.join(':');
        $('.sp-payment__invoice_end').text(result_time_view);
      }, 1000);
    }

    function correct_end_time(time_number) {
      return ('0' + time_number).substr(-2);
    }

    function open_validate_pay() {
      let safe_pay_popup = $('.sp-popup');
      if (safe_pay_popup.hasClass('sp-popup')) {
        safe_pay_popup.show();
        $('.sp-popup__button--check').click();
      } else {
        let popup_block = '';
        popup_block += '<div class="sp-popup">';
        popup_block += '<div class="sp-popup__block">';
        popup_block += '<div class="sp-popup__exit">x</div>';
        popup_block += '<h3 class="sp-popup__title">%SP_CHECK_TITLE%</h3>';
        popup_block += '<div class="sp-popup__loader">';
        popup_block += '<img src="/catalog/view/theme/default/image/payment/safepay/loader.gif" alt="Loading...">';
        popup_block += '</div>';
        popup_block += '<div class="sp-popup__result"></div>';
        popup_block += '<a class="sp-popup__button sp-popup__button--check" href="#">%SP_CHECK_LINK%</a>';
        popup_block += '<p class="sp-popup__desc">%SP_CHECK_DESC%</p>';
        popup_block += '</div>';
        popup_block += '</div>';

        $('body').prepend(popup_block);
        $('.sp-popup__button--check').click();
      }
    }

    if ($('.sp-payment__invoice_end').
        hasClass('sp-payment__invoice_end')) {
      timer();
    }

    $('body').on('click', '.sp-popup, .sp-popup__exit', function() {
      $('.sp-popup').hide();
    });

    $('body').on('click', '.sp-popup__block', function(e) {
      e.stopPropagation();
    });

    $('.sp-payment__submit').click(function(e) {
      e.preventDefault();
      open_validate_pay();
    });

    $('body').on('click', '.sp-popup__button--check', function(e) {
      e.preventDefault();

      if (!$(this).hasClass('sp-popup__button--disabled')) {

        let sign = $('.sp-payment__submit').data('invoice_signature');
        let order_id = $('.sp-payment__submit').data('invoice_order_id');
        let data = {
          'token': '%SP_TOKEN%',
          'signature': sign,
          'order_id': order_id,
        };
        if (sign !== undefined && sign !== '' && sign.length > 0) {
          $.ajax({
            method: 'post',
            url: 'index.php?route=extension/payment/safepay/check',
            data: data,
            cache: false,
            beforeSend: function() {
              $('.sp-popup__result').html('').hide();
              $('.sp-popup__loader').show();
              $('.sp-popup__button--check').
                  addClass('sp-popup__button--disabled');
            },
            success: function(data) {
              setTimeout(function() {
                $('.sp-popup__loader').hide();
                let html_result = '';
                if (data === false) {
                  html_result += '<img src="/catalog/view/theme/default/image/payment/safepay/false.png">';
                  html_result += '<p>%SP_CHECK_FALSE%</p>';
                } else {
                  html_result += '<img src="/catalog/view/theme/default/image/payment/safepay/true.png">';
                  html_result += '<p>%SP_CHECK_TRUE%</p>';
                  setTimeout(function() {
                    location.reload();
                  }, 1000);
                }
                $('.sp-popup__result').html(html_result).fadeIn('slow');
                $('.sp-popup__button--check').
                    removeClass('sp-popup__button--disabled');
              }, 300);
            },
            error: function(xhr, ajaxOptions, thrownError) {
              console.log(thrownError + '\r\n' + xhr.statusText + '\r\n' +
                  xhr.responseText);
            },
            dataType: 'json',
          });
        }
      }
    });
  });
})(jQuery);