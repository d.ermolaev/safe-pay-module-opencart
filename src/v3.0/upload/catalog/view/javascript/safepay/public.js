(function($) {
  'use strict';
  $(document).ready(function() {

    const USER_OS_LINK = get_user_os();

    function get_user_os() {

      let user_agent = window.navigator.userAgent;
      if (user_agent.indexOf('Android') !== -1 ||
          user_agent.indexOf('android') !== -1) return 'APP_ANDROID';
      if (user_agent.indexOf('IPhone') !== -1 ||
          user_agent.indexOf('IPad') !== -1) return 'APP_IOS';
      return 'PAY_URL';

    }

    function open_bank_list(all_recipient) {
      let safe_pay_popup = $('.sp-popup');
      if (safe_pay_popup.hasClass('sp-popup')) {
        safe_pay_popup.show();
      } else {
        let popup_block = '';
        let obj_recipient = jQuery.parseJSON(all_recipient);
        let i = 0, obj_current = {};
        popup_block += '<div class="sp-popup">';
        popup_block += '<div class="sp-popup__block">';
        popup_block += '<div class="sp-popup__exit">x</div>';
        popup_block += '<h3 class="sp-popup__title">%SP_CHANGE_BANK%</h3>';
        popup_block += '<div class="sp-popup__recipients">';
        obj_recipient.forEach(function(e) {
          let activ_link = '';
          if (i === 0) {
            obj_current = e;
            activ_link = ' sp-popup__recipient--active';
          }
          popup_block += '<div class="sp-popup__recipient' + activ_link +
              '" data-attribute="' + e.ATTRIBUTE + '" data-app_android="' +
              e.APP_ANDROID + '" data-app_ios="' + e.APP_IOS +
              '" data-pay_url="' + e.PAY_URL + '">';
          popup_block += '<img src="' + e.PICTURE_URL + '" alt="' +
              e.NAME + '" title="' + e.NAME + '">';
          popup_block += '</div>';
          i++;
        });
        popup_block += '</div>';
        popup_block += '<p class="sp-popup__vote">%SP_VOTE%</p>';
        popup_block += '%SP_CHECK_PHONE%';
        popup_block += '<a class="sp-popup__button sp-popup__button--send" href="' +
            obj_current[USER_OS_LINK] + '" target="_blank" data-recipient="' +
            obj_current['ATTRIBUTE'] + '">%SP_LINK_BANK%</a>';
        popup_block += '%SP_LINK_QR%';
        popup_block += '<p class="sp-popup__desc">%SP_INSTR%</p>';
        popup_block += '</div>';
        popup_block += '</div>';
        $('body').prepend(popup_block);
      }
    }

    $('.sp-payment__submit').click(function(e) {
      e.preventDefault();
      let all_recipient = $(this).
          parents('.sp-payment__form').
          find('input[name="ALL_RECIPIENT"]').
          val();
      open_bank_list(all_recipient);
      if ($('.sp-popup__phone').hasClass('sp-popup__phone')) {
        $('.sp-popup__button--send').addClass('sp-popup__button--disabled');

        $('.sp-popup__phone input').on('change input keyup', function() {
          let phone = $(this).val();
          phone = phone.replace(/\D+/g, '');
          if (phone.length >= 10) {
            $('.sp-popup__button--send').
                removeClass('sp-popup__button--disabled');
            $('.sp-payment__form').find('input[name="userPhone"]').val(phone);
          } else {
            $('.sp-popup__button--send').addClass('sp-popup__button--disabled');
            $('.sp-payment__form').find('input[name="userPhone"]').val('');
          }
        });
      }
    });

    $('body').on('click', '.sp-popup, .sp-popup__exit', function() {
      $('.sp-popup').hide();
    });

    $('body').on('click', '.sp-popup__block', function(e) {
      e.stopPropagation();
    });

    $('body').on('click', '.sp-popup__recipient', function() {
      if (!$(this).hasClass('sp-popup__recipient--active')) {
        $('.sp-popup__recipient--active').
            removeClass('sp-popup__recipient--active');
        $(this).addClass('sp-popup__recipient--active');
        $('.sp-popup__button--send').
            attr('href', $(this).data(USER_OS_LINK.toLowerCase())).
            attr('data-recipient', $(this).data('attribute'));
      }
    });

    $('body').on('click', '.sp-popup__button--send', function(e) {
      if (!$(this).hasClass('sp-popup__button--disabled')) {
        let sp_form = $('.sp-payment__form');
        let data = {
          'token': '%SP_TOKEN%',
          'recipient': $(this).attr('data-recipient'),
          'order_date': sp_form.find('input[name="order_date"]').val(),
          'order_num': sp_form.find('input[name="order_num"]').val(),
          'userPhone': sp_form.find('input[name="userPhone"]').val(),
          'curr': sp_form.find('input[name="curr"]').val(),
          'sum': sp_form.find('input[name="sum"]').val(),
          'expire': sp_form.find('input[name="expire"]').val(),
          'title': sp_form.find('input[name="title"]').val(),
          'description': sp_form.find('input[name="description"]').val(),
        };
        $.ajax({
          url: 'index.php?route=extension/payment/safepay/confirm',
          type: 'POST',
          data: data,
          cache: false,
          beforeSend: function() {
            window.onbeforeunload = function() {
              return 'Данные не сохранены. Точно перейти?';
            };
            $('.sp-popup__button--send').
                removeClass('sp-popup__button--error').
                addClass('sp-popup__button--disabled').
                text('%SP_SEND_ORDER%');
            $('body').
                append(
                    '<div id="before_send_SP" style="position: fixed;width: 100%;height:100%;z-index: 99999999999999;top:0;left:0;"></div>');
          },
          success: function(d) {
            if (d.STATUS === 'OK') {
              window.onbeforeunload = function() {
              };
              location.href = '%SP_REDIRECT%';
            } else {
              $('#before_send_SP').remove();
              $('.sp-popup__button--send').
                  removeClass('sp-popup__button--disabled').
                  addClass('sp-popup__button--error').
                  text('%SP_SEND_ERROR%');
            }
          },
          error: function(xhr, ajaxOptions, thrownError) {
            console.log(thrownError + '\r\n' + xhr.statusText + '\r\n' +
                xhr.responseText);
          },
          dataType: 'json',
        });
      } else {
        e.preventDefault();
      }
    });

    $('body').on('click', '.sp-popup__button--qr', function(e) {
      if (!$(this).hasClass('sp-popup__button--disabled')) {
        let sp_form = $('.sp-payment__form');
        let data = {
          'token': '%SP_TOKEN%',
          'order_num': sp_form.find('input[name="order_num"]').val(),
        };
        $.ajax({
          url: 'index.php?route=extension/payment/safepay/confirm_qr',
          type: 'POST',
          data: data,
          cache: false,
          beforeSend: function() {
            window.onbeforeunload = function() {
              return 'Данные не сохранены. Точно перейти?';
            };
            $('.sp-popup__button--qr').
                removeClass('sp-popup__button--error').
                addClass('sp-popup__button--disabled').
                text('%SP_SEND_ORDER%');
            $('body').
                append(
                    '<div id="before_send_SP" style="position: fixed;width: 100%;height:100%;z-index: 99999999999999;top:0;left:0;"></div>');
          },
          success: function(d) {
            if (d.status === 'OK') {
              window.onbeforeunload = function() {
              };
              location.href = '%SP_URL_QR%';
            } else {
              $('#before_send_SP').remove();
              $('.sp-popup__button--qr').
                  removeClass('sp-popup__button--disabled').
                  addClass('sp-popup__button--error').
                  text('%SP_SEND_ERROR%');
            }
          },
          error: function(xhr, ajaxOptions, thrownError) {
            console.log(thrownError + '\r\n' + xhr.statusText + '\r\n' +
                xhr.responseText);
          },
          dataType: 'json',
        });
      } else {
        e.preventDefault();
      }
    });
  });
})(jQuery);