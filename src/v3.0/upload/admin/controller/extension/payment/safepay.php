<?php

use SafePay\Blockchain\Options;
use SafePay\Blockchain\Base;
use SafePay\Blockchain\DBServer;
use SafePay\Blockchain\DBInvoice;
use SafePay\Blockchain\Process;
use Alcohol\ISO4217;

class ControllerExtensionPaymentSafepay extends Controller
{
    public function __construct($registry)
    {
        parent::__construct($registry);

        $this->load->language('extension/payment/safepay');
        $this->load->library('safepay');
    }

    /**
     * Страница настроек модуля в админ-панели
     */
    public function index()
    {
        $this->load->model('extension/payment/safepay');

        if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
            if (empty($this->request->post['payment_safepay_last_t']) || empty($this->request->post['payment_safepay_last_l'])) {
                $base = new Base();
                $lastBlock = $base->getLastBlockData();
                $this->request->post['payment_safepay_last_t'] = $lastBlock['lastBlockTest'];
                $this->request->post['payment_safepay_last_l'] = $lastBlock['lastBlock'];
            }
            $this->model_extension_payment_safepay->save_settings();
            $this->session->data['success'] = 'Настройки сохранены';
            $this->response->redirect($this->url->link('marketplace/extension',
                'user_token=' . $this->session->data['user_token'] . '&type=payment', true));
        }

        $data = array();
        $data += $this->model_extension_payment_safepay->load_settings();
        $data += $this->get_breadcrumbs();
        $data += $this->get_default_settings();
        $data += $this->get_geolocation_zone();
        $data += $this->get_warning_message();
        $data += $this->get_button();
        $data += $this->get_payment_status();

        //title
        $this->document->setTitle($this->language->get('heading_title'));

        //template
        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        //source
        $data['link_css'] = '/admin/view/stylesheet/safepay/admin.css?t=' . time();
        $data['link_js_base58'] = '/admin/view/javascript/safepay/Base58.js';
        $data['link_js_sha256'] = '/admin/view/javascript/safepay/sha256.js';
        $data['link_js_ripemd160'] = '/admin/view/javascript/safepay/ripemd160.js';
        $data['link_js_eralib'] = '/admin/view/javascript/safepay/eralib.js';

        //sp_db
        $data['sp_all_server'] = DBServer::getAllServers();
        $data['sp_all_invoice'] = array();
        if ($DBInvoice = DBInvoice::getAllInvoice()) {
            $iso4217 = new ISO4217();
            $DBInvoice = array_reverse($DBInvoice);
            foreach ($DBInvoice as $invoice_item) {
                $message = unserialize($invoice_item->CREATOR);
                $data['sp_all_invoice'][$invoice_item->ID]['ID'] = $invoice_item->ID;
                $data['sp_all_invoice'][$invoice_item->ID]['DATE_CREATED'] = date('d.m.Y H:i',
                    $invoice_item->DATE_CREATED);
                $data['sp_all_invoice'][$invoice_item->ID]['STATUS'] = $invoice_item->STATUS;
                $data['sp_all_invoice'][$invoice_item->ID]['BANK_ID'] = $invoice_item->BANK_ID;
                $data['sp_all_invoice'][$invoice_item->ID]['PAY_NUM'] = $invoice_item->PAY_NUM;
                $data['sp_all_invoice'][$invoice_item->ID]['AMOUNT'] = $message['sum'];
                $data['sp_all_invoice'][$invoice_item->ID]['CURRENCY'] = $iso4217->getByNumeric($message['curr']);
            }
        }
        $data['sp_invoice_status'] = array(
            'active'   => $this->language->get('sp_status_active'),
            'finish'   => $this->language->get('sp_status_finish'),
            'waiting'  => $this->language->get('sp_status_waiting'),
            'canceled' => $this->language->get('sp_status_canceled'),
            'expired'  => $this->language->get('sp_status_expired'),
        );

        $data['token'] = $this->session->data['user_token'];

        $code_js = file_get_contents(Options::getSiteUrl() . '/admin/view/javascript/safepay/admin.js?t=' . time());
        $code_js = str_replace('%TOKEN%', $this->session->data['user_token'], $code_js);
        $code_js = str_replace('%WARNING_KEY%', $this->language->get('sp_warning_key'), $code_js);
        $code_js = str_replace('%SP_SERVER_DEL%', $this->language->get('sp_server_del'), $code_js);
        $data['code_js'] = $code_js;
        //output
        $this->response->setOutput($this->load->view('extension/payment/safepay', $data));
    }

    /**
     * Функция с действиями при установке модуля
     */
    public function install()
    {
        $this->load->model('extension/payment/safepay');
        $this->model_extension_payment_safepay->create_table();

        $this->load->model('setting/setting');
        $this->model_setting_setting->editSetting('payment_safepay', array('payment_safepay_status' => 0));
    }

    /**
     * Функция с действиями при удалении модуля
     */
    public function uninstall()
    {
        $this->load->model('extension/payment/safepay');
        $this->model_extension_payment_safepay->delete_table();

        $this->load->model('setting/setting');
        $this->model_setting_setting->editSetting('payment_safepay', array('payment_safepay_status' => 0));
    }

    /**
     * Генерация ключей
     *
     * @throws \SafePay\Blockchain\Sodium\SodiumException
     */
    public function generate_key()
    {
        $base = new Base();
        $account = $base->generateAccount();
        unset($account['seed'], $account['accountSeed'], $account['numAccount']);

        die(json_encode($account));
    }

    /**
     * Добавление нового сервера
     */
    public function add_server()
    {
        if (empty($_POST['URL_SERVER']) || empty($_POST['TYPE_SERVER'])) {
            $result['status'] = 'error';
            $result['message'] = $this->language->get('sp_server_valid_all');

            die(json_encode($result));
        }
        $url_server = $this->esc_html($_POST['URL_SERVER']);
        $type_server = $this->esc_html($_POST['TYPE_SERVER']);
        if (DBServer::getServerByURL(strtolower($url_server)) !== false) {
            $result['status'] = 'error';
            $result['message'] = $this->language->get('sp_server_valid_isset');

            die(json_encode($result));
        }
        if ($type_server != 'live' && $type_server != 'test') {
            $result['status'] = 'error';
            $result['message'] = $this->language->get('sp_server_valid_type');

            die(json_encode($result));
        }
        if (!preg_match("~^[a-zA-Zа-яА-Я0-9-\.\:\/]*$~", $url_server)) {
            $result['status'] = 'error';
            $result['message'] = $this->language->get('sp_server_valid_url');

            die(json_encode($result));
        }

        $server_data = array(
            'URL_SERVER'  => strtolower($url_server),
            'TYPE_SERVER' => $type_server
        );
        if ($id = DBServer::addServer($server_data)) {
            $result['status'] = 'success';
            $result['message'] = $this->language->get('sp_server_valid_success');
            $result['server'] = array(
                'id'   => $id,
                'url'  => $url_server,
                'type' => $type_server
            );
        } else {
            $result['status'] = 'error';
            $result['message'] = $this->language->get('sp_server_valid_error');
        }

        die(json_encode($result));
    }

    /**
     * Удаление сервера
     */
    public function del_server()
    {
        $server_id = $this->esc_html($_POST['server_id']);
        if (filter_var($server_id, FILTER_VALIDATE_INT)) {
            DBServer::deleteServer($server_id);
            $result['status'] = 'success';
            $result['message'] = $this->language->get('sp_server_del_success');
        } else {
            $result['status'] = 'error';
            $result['message'] = $this->language->get('sp_server_del_error');
        }

        die(json_encode($result));
    }

    /**
     * Обновление статусов транзакций
     */
    public function update_transactions()
    {
        $process = new Process();
        $process->resultPay();
        header("Location: /admin/index.php?route=extension/payment/safepay&user_token=" . $_GET['user_token']);
    }

    /**
     * Получение хлебных крошек
     *
     * @return array
     */
    private function get_breadcrumbs()
    {
        $data = array();
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('sp_payment'),
            'href' => $this->url->link('marketplace/extension',
                'user_token=' . $this->session->data['user_token'] . '&type=payment', true)
        );
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('extension/payment/safepay', 'user_token=' . $this->session->data['user_token'],
                true)
        );
        return $data;
    }

    /**
     * Получение настроек модуля по умолчанию
     *
     * @return array
     */
    private function get_default_settings()
    {
        $data = array();
        $data['payment_safepay_interval_server_default'] = 20;
        $data['payment_safepay_interval_pay_default'] = 5;
        $data['payment_safepay_interval_pay_e_default'] = 12;
        $data['payment_safepay_temp_title_default'] = $this->language->get('sp_extend_temp_title_default');
        $data['payment_safepay_temp_desc_default'] = $this->language->get('sp_extend_temp_desc_default');
        $data['payment_safepay_qr_title_default'] = $this->language->get('sp_qr_title_default');

        return $data;
    }

    /**
     * Получение списка гео-зон
     *
     * @return array
     */
    private function get_geolocation_zone()
    {
        $data = array();
        $this->load->model('localisation/geo_zone');
        $data['geoZones'] = $this->model_localisation_geo_zone->getGeoZones();

        return $data;
    }

    /**
     * Получение списка ошибок
     *
     * @return array
     */
    private function get_warning_message()
    {
        $data = array();
        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        return $data;
    }

    /**
     * Получение данных по кнопкам сохранения и перехода в раздел модулей
     *
     * @return array
     */
    private function get_button()
    {
        $data = array();
        $data['action'] = $this->url->link('extension/payment/safepay',
            'user_token=' . $this->session->data['user_token'], true);
        $data['cancel'] = $this->url->link('marketplace/extension',
            'user_token=' . $this->session->data['user_token'] . '&type=payment', true);

        return $data;
    }

    /**
     * Получение статуса платежной системы
     *
     * @return array
     */
    private function get_payment_status()
    {
        $data = array();
        if (isset($this->request->post['payment_safepay_status'])) {
            $data['payment_safepay_status'] = $this->request->post['payment_safepay_status'];
        } else {
            $data['payment_safepay_status'] = $this->config->get('payment_safepay_status');
        }

        return $data;
    }

    /**
     * Чистка входящих данных
     *
     * @param string $text
     * @return string
     */
    private function esc_html($text)
    {
        $text = trim($text);
        $text = strip_tags($text);
        $text = htmlspecialchars($text, ENT_QUOTES);

        return $text;
    }
}