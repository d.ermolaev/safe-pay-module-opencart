<?php

class ModelExtensionPaymentSafepay extends Model
{
    /**
     * Создание таблиц при добавлении модуля
     */
    public function create_table()
    {
        //server
        $this->create_table_server();
        $this->insert_data_server();
        //invoice
        $this->create_table_invoice();
        //recipient
        $this->create_table_recipient();
        $this->insert_data_recipient();
    }

    /**
     * Создание таблицы серверов
     */
    private function create_table_server()
    {
        $sql = "CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "safe_pay_server` (
                  ID int(18) NOT NULL AUTO_INCREMENT,
                  TYPE_SERVER varchar(10) NOT NULL,
                  URL_SERVER varchar(255) NOT NULL,
                  TIME_UPDATE varchar(13) NOT NULL DEFAULT 0,
                  PRIMARY KEY (ID)
            );";

        $this->db->query($sql);
    }

    /**
     * Добавление данных в таблицу серверов
     */
    private function insert_data_server()
    {
        $array = array(
            array('type' => 'test', 'url' => 'http://89.235.184.229:9067'),
            array('type' => 'live', 'url' => 'http://138.68.232.232:9047'),
            array('type' => 'live', 'url' => 'http://138.68.186.214:9047'),
            array('type' => 'live', 'url' => 'http://138.68.225.51:9047'),
            array('type' => 'live', 'url' => 'http://46.101.14.242:9047'),
            array('type' => 'live', 'url' => 'http://138.197.135.122:9047'),
            array('type' => 'live', 'url' => 'http://207.154.249.81:9047'),
            array('type' => 'live', 'url' => 'http://207.154.214.55:9047'),
            array('type' => 'live', 'url' => 'http://138.197.143.167:9047'),
            array('type' => 'live', 'url' => 'http://206.81.0.11:9047'),
            array('type' => 'live', 'url' => 'http://193.42.145.120:9047'),
            array('type' => 'live', 'url' => 'http://204.48.28.103:9047'),
            array('type' => 'live', 'url' => 'http://explorer.erachain.org:9047')
        );

        foreach ($array as $item) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "safe_pay_server (TYPE_SERVER, URL_SERVER, TIME_UPDATE) VALUES ('" . $item['type'] . "','" . $item['url'] . "','0');");
        }
    }

    /**
     * Создание таблицы инвойсов
     */
    private function create_table_invoice()
    {
        $sql = "CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "safe_pay_invoice` (
                ID int(18) NOT NULL AUTO_INCREMENT,
                STATUS varchar(10) NOT NULL,
                RECIPIENT varchar(10) NOT NULL,
                BANK_ID varchar(100) NOT NULL,
                EXPIRE int(10) NOT NULL,
                PAY_NUM int(11) NOT NULL,
                IS_TEST int(1) NOT NULL,
                DATE_CREATED int(10) NOT NULL DEFAULT 0,
                DATE_UPDATED int(10) NOT NULL DEFAULT 0,
                CREATOR text NOT NULL,
                SITE_URL varchar(255) NOT NULL,
                PRIMARY KEY (ID)
            );";

        $this->db->query($sql);
    }

    /**
     * Создание таблицы реципиентов
     */
    private function create_table_recipient()
    {
        $sql = "CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "safe_pay_recipient` (
                ID int(10) NOT NULL AUTO_INCREMENT,
                NAME varchar(100) NOT NULL,
                ATTRIBUTE varchar(10) NOT NULL,
                BILD varchar(50) NOT NULL,
                PUBLIC_KEY varchar(50) NOT NULL,
                PAY_URL varchar(255) NOT NULL,
                APP_ANDROID varchar(255),
                APP_IOS varchar(255),
                PICTURE_URL varchar(255) NOT NULL,
                PRIMARY KEY (ID)
            );";

        $this->db->query($sql);
    }

    /**
     * Добавление данных в таблицу реципиентов
     */
    private function insert_data_recipient()
    {
        $sql = '';
        $array = array(
            array(
                'NAME'        => 'ТрансСтройБанк',
                'ATTRIBUTE'   => 'tsbnk',
                'BILD'        => '7Dpv5Gi8HjCBgtDN1P1niuPJQCBQ5H8Zob',
                'PUBLIC_KEY'  => '2M9WSqXrpmRzaQkxjcWKnrABabbwqjPbJktBDbPswgL7',
                'PAY_URL'     => 'https://online.transstroybank.ru/',
                'APP_ANDROID' => 'https://play.google.com/store/apps/details?id=com.isimplelab.ibank.tsbank',
                'APP_IOS'     => 'https://apps.apple.com/ru/app/тсб-онлайн/id723491575?mt=8',
                'PICTURE_URL' => '/catalog/view/theme/default/image/payment/safepay/tsbnk.png',
            )
        );

        foreach ($array as $item) {
            $sql .= "INSERT INTO " . DB_PREFIX . "safe_pay_recipient (NAME, ATTRIBUTE, BILD, PUBLIC_KEY, PAY_URL, APP_ANDROID, APP_IOS, PICTURE_URL) VALUES ('" . $item['NAME'] . "', '" . $item['ATTRIBUTE'] . "', '" . $item['BILD'] . "', '" . $item['PUBLIC_KEY'] . "', '" . $item['PAY_URL'] . "', '" . $item['APP_ANDROID'] . "', '" . $item['APP_IOS'] . "', '" . $item['PICTURE_URL'] . "');";
        }

        $this->db->query($sql);
    }

    /**
     * Удаление таблиц, при удалении модуля
     */
    public function delete_table()
    {
        $sql = "DROP TABLE IF EXISTS " . DB_PREFIX . "safe_pay_server, " . DB_PREFIX . "safe_pay_invoice, " . DB_PREFIX . "safe_pay_recipient;";
        $this->db->query($sql);
    }

    /**
     * Сохранение настроек модуля
     */
    public function save_settings()
    {
        $this->load->model('setting/setting');
        $this->model_setting_setting->editSetting('payment_safepay', $this->request->post);
    }

    /**
     * Получение настроек модуля
     *
     * @return array
     */
    public function load_settings()
    {
        return array(
            'payment_safepay_status'               => $this->config->get('payment_safepay_status'),
            'payment_safepay_geo'                  => $this->config->get('payment_safepay_geo'),
            'payment_safepay_public_key'           => $this->config->get('payment_safepay_public_key'),
            'payment_safepay_private_key'          => $this->config->get('payment_safepay_private_key'),
            'payment_safepay_bild'                 => $this->config->get('payment_safepay_bild'),
            'payment_safepay_interval_server'      => $this->config->get('payment_safepay_interval_server'),
            'payment_safepay_interval_pay'         => $this->config->get('payment_safepay_interval_pay'),
            'payment_safepay_interval_pay_expired' => $this->config->get('payment_safepay_interval_pay_expired'),
            'payment_safepay_test'                 => $this->config->get('payment_safepay_test'),
            'payment_safepay_last_t'               => $this->config->get('payment_safepay_last_t'),
            'payment_safepay_last_l'               => $this->config->get('payment_safepay_last_l'),
            'payment_safepay_temp_title'           => $this->config->get('payment_safepay_temp_title'),
            'payment_safepay_temp_desc'            => $this->config->get('payment_safepay_temp_desc'),
            'payment_safepay_cron_pay'             => $this->config->get('payment_safepay_cron_pay'),
            'payment_safepay_cron_server'          => $this->config->get('payment_safepay_cron_server'),
            'payment_safepay_qr_status'            => $this->config->get('payment_safepay_qr_status'),
            'payment_safepay_qr_name'              => $this->config->get('payment_safepay_qr_name'),
            'payment_safepay_qr_number'            => $this->config->get('payment_safepay_qr_number'),
            'payment_safepay_qr_inn'               => $this->config->get('payment_safepay_qr_inn'),
            'payment_safepay_qr_title'             => $this->config->get('payment_safepay_qr_title')
        );
    }
}