<?php /** @noinspection PhpUndefinedVariableInspection */
echo $header; ?>
    <div class="container">
        <ul class="breadcrumb">
            <?php foreach ($breadcrumbs as $breadcrumb): ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            <?php endforeach; ?>
        </ul>
        <div id="content">
            <h1><?php echo $sp_success_title; ?></h1>
            <div class="sp-payment">
                <div class="sp-payment__title">
                    <h3><?php echo $text_title; ?></h3>
                    <!--suppress HtmlUnknownTarget -->
                    <img src="/catalog/view/theme/default/image/payment/safepay/safepay_logo.png"
                         alt="<?php echo $text_title; ?>">
                </div>

                <?php if ($sp_type_success == 'check'): ?>
                    <p class="sp-payment__desc"><?php echo $sp_check_pay; ?></p>
                    <p class="sp-payment__desc"><?php echo $sp_success_expire; ?> <span class="sp-payment__invoice_end"
                                                                                        data-invoice_end="<?php echo $sp_get_expire; ?>"></span>
                    </p>
                    <form method="POST" class="sp-payment__form">

                        <input type="submit" class="sp-payment__submit btn btn-primary" value="<?php echo $sp_check; ?>"
                               data-invoice_status="false"
                               data-invoice_signature="<?php echo $sp_get_signature; ?>"
                               data-invoice_order_id="<?php echo $sp_get_order_id; ?>"/>
                    </form>
                <?php elseif ($sp_type_success == 'send'): ?>
                    <p class="sp-payment__desc"><?php echo $sp_send_pay; ?></p>
                    <p class="sp-payment__desc sp-payment__desc--order"><?php echo $sp_send_pay_order; ?> <?php echo $sp_get_order_id; ?></p>
                    <p class="sp-payment__desc sp-payment__desc--amount"><?php echo $sp_send_pay_amount; ?> <?php echo $sp_get_amount; ?></p>
                    <form method="POST" class="sp-payment__form">
                        <?php foreach ($hide_param as $key => $value):
                            $key = trim($key);
                            $key = strip_tags($key);
                            $key = htmlspecialchars($key, ENT_QUOTES);

                            $value = trim($value);
                            $value = strip_tags($value);
                            $value = htmlspecialchars($value, ENT_QUOTES);
                            ?>
                            <?php if ($key == 'ALL_RECIPIENT'): ?>
                            <input type="hidden" name="<?php echo $key; ?>" value="<?php echo $value; ?>" disabled/>
                        <?php else: ?>
                            <input type="hidden" name="<?php echo $key; ?>" value="<?php echo $value; ?>"/>
                        <?php endif; ?>
                        <?php endforeach; ?>
                        <input type="submit" class="sp-payment__submit btn btn-primary" value="<?php echo $sp_pay; ?>"
                               data-invoice_status="true"
                               data-invoice_signature="false"/>
                    </form>
                <?php elseif ($sp_type_success == 'finish'): ?>
                    <p class="sp-payment__desc"><?php echo $sp_true_pay; ?></p>
                <?php else: ?>
                    <p class="sp-payment__desc"><?php echo $sp_false_pay; ?></p>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <script type="text/javascript"><?php echo $code_js; ?></script>
    <style><?php echo $code_css; ?></style>
<?php echo $footer; ?>