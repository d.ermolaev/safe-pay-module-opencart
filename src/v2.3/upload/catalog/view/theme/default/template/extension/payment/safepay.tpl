<?php /** @noinspection PhpUndefinedVariableInspection */ ?>
<div class="sp-payment">
    <div class="sp-payment__title">
        <h3><?php echo $text_title; ?></h3><?php echo $text_title; ?>
        <!--suppress HtmlUnknownTarget -->
        <img src="/catalog/view/theme/default/image/payment/safepay/safepay_logo.png" alt="<?php echo $text_title; ?>">
    </div>

    <p class="sp-payment__desc"><?php echo $sp_send_pay; ?></p>
    <form method="POST" class="sp-payment__form">
        <?php foreach ($hide_param as $key => $value):
            $key = trim($key);
            $key = strip_tags($key);
            $key = htmlspecialchars($key, ENT_QUOTES);

            $value = trim($value);
            $value = strip_tags($value);
            $value = htmlspecialchars($value, ENT_QUOTES);
            ?>
            <?php if ($key == 'ALL_RECIPIENT'): ?>
            <input type="hidden" name="<?php echo $key; ?>" value="<?php echo $value; ?>" disabled/>
        <?php else: ?>
            <input type="hidden" name="<?php echo $key; ?>" value="<?php echo $value; ?>"/>
        <?php endif; ?>
        <?php endforeach; ?>
        <input type="submit" class="sp-payment__submit btn btn-primary" value="<?php echo $sp_pay; ?>"
               data-invoice_status="true"
               data-invoice_signature="false"/>
    </form>
</div>

<style><?php echo $code_css; ?></style>
<script type="text/javascript"><?php echo $code_js; ?></script>