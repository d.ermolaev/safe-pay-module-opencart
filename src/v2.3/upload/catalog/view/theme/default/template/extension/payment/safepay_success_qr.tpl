<?php /** @noinspection PhpUndefinedVariableInspection */
echo $header; ?>
    <div class="container">
        <ul class="breadcrumb">
            <?php foreach ($breadcrumbs as $breadcrumb): ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            <?php endforeach; ?>
        </ul>
        <div id="content">
            <h1><?php echo $sp_success_title; ?></h1>
            <div class="sp-payment">
                <div class="sp-payment__title">
                    <h3><?php echo $text_title; ?></h3>
                    <!--suppress HtmlUnknownTarget -->
                    <img src="/catalog/view/theme/default/image/payment/safepay/safepay_logo.png"
                         alt="<?php echo $text_title; ?>">
                </div>

                <?php if ($sp_type_success == 'true'): ?>
                    <div class="sp-payment-qr">
                        <div class="sp-payment-qr__code">
                            <img src="<?php echo $sp_qr; ?>" alt="QR SAFE PAY">
                        </div>
                        <div class="sp-payment-qr__desc">
                            <p class="sp-payment__desc"><?php echo $sp_qr_pay; ?></p>
                            <p class="sp-payment__desc"><?php echo $sp_qr_pay_false; ?></p>
                            <a class="sp-popup__button"
                               href="<?php echo $sp_url_bank; ?>"><?php echo $sp_link_bank_text; ?></a>
                        </div>
                    </div>
                <?php elseif ($sp_type_success == 'finish'): ?>
                    <p class="sp-payment__desc"><?php echo $sp_true_pay; ?></p>
                <?php else: ?>
                    <p class="sp-payment__desc"><?php echo $sp_false_pay; ?></p>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <script type="text/javascript"><?php echo $code_js; ?></script>
    <style><?php echo $code_css; ?></style>
<?php echo $footer; ?>