<?php

use Alcohol\ISO4217;
use SafePay\Blockchain\Cron;
use SafePay\Blockchain\DBInvoice;
use SafePay\Blockchain\DBRecipient;
use SafePay\Blockchain\Options;
use SafePay\Blockchain\Process;

class ControllerExtensionPaymentSafepay extends Controller
{
    private $token;

    public function __construct($registry)
    {
        parent::__construct($registry);

        $this->load->model('extension/payment/safepay');
        $this->load->language('extension/payment/safepay');
        $this->load->library('safepay');
        $this->load->library('phpqrcode');

        $this->token = $this->genCsrf();
    }

    /**
     * вывод метода оплаты на странице оформления заказа
     *
     * @return mixed
     */
    public function index()
    {
        $data['button_confirm'] = $this->language->get('button_confirm');
        $data['button_back'] = $this->language->get('button_back');

        $this->load->model('checkout/order');
        $order = $this->model_checkout_order->getOrder($this->session->data['order_id']);

        $data += $this->getSendOrderData($order);

        $data += $this->load->language('extension/payment/safepay');

        $pathToView = (property_exists('Config', 'config_template') ? '' : $this->config->get('config_template'));
        $data['action'] = '';

        $code_css = file_get_contents(Options::getSiteUrl() . '/catalog/view/theme/default/stylesheet/safepay/public.css?t=' . time());
        $data['code_css'] = $code_css;
        $data['code_js'] = $this->getCodeSend($order);

        return $this->load->view($pathToView . 'extension/payment/safepay.tpl', $data);
    }

    /**
     * Проверка данных и отправка телеграммы
     *
     * @throws \SafePay\Blockchain\Sodium\SodiumException
     */
    public function confirm()
    {
        if (!empty($_POST) && $this->session->data['payment_method']['code'] == 'safepay') {

            $error = array();

            $recipient = $this->esc_html($_POST['recipient']);
            if (!preg_match("~^[a-zA-Z0-9]*$~", $recipient)) {
                $error['data'][] = $this->language->get('sp_valid_recipient');
            }

            $order_date = $this->esc_html($_POST['order_date']);
            if (!filter_var($order_date, FILTER_VALIDATE_INT)) {
                $error['data'][] = $this->language->get('sp_valid_date');
            }

            $order_num = $this->esc_html($_POST['order_num']);
            if (!filter_var($order_num, FILTER_VALIDATE_INT)) {
                $error['data'][] = $this->language->get('sp_valid_order_id');
            }

            $userPhone = $this->esc_html($_POST['userPhone']);
            if (!filter_var($userPhone, FILTER_VALIDATE_INT)) {
                $error['data'][] = $this->language->get('sp_valid_phone');
            }

            $curr = $this->esc_html($_POST['curr']);
            if (!filter_var($curr, FILTER_VALIDATE_INT)) {
                $error['data'][] = $this->language->get('sp_valid_currency');
            }

            $sum = $this->esc_html($_POST['sum']);
            if (!preg_match("~^[0-9-\.\,]*$~", $sum)) {
                $error['data'][] = $this->language->get('sp_valid_amount');
            }

            $expire = $this->esc_html($_POST['expire']);
            if (!filter_var($expire, FILTER_VALIDATE_INT)) {
                $error['data'][] = $this->language->get('sp_valid_expire');
            }

            $title = $this->esc_html($_POST['title']);
            $description = $this->esc_html($_POST['description']);

            if (isset($error['data']) && count($error['data']) > 0) {
                $error['status'] = 'error';

                die($error);
            }

            $result = new Process();
            $array = array(
                'recipient'   => $recipient,
                'order_date'  => $order_date,
                'order_num'   => $order_num,
                'userPhone'   => $userPhone,
                'curr'        => $curr,
                'sum'         => $sum,
                'expire'      => $expire,
                'title'       => $title,
                'description' => $description
            );
            if ($res = $result->initPay($array, $recipient)) {
                $this->load->model('checkout/order');

                if ($this->model_checkout_order->getOrder($order_num)['order_status_id'] == 0) {
                    $this->model_checkout_order->addOrderHistory($order_num, 1);
                }

                $this->cart->clear();
            }
            $json = json_encode($res);
        } else {
            $error['status'] = 'error';
            $json = json_encode($error);
        }

        die($json);
    }

    /**
     * Подтверждение заказа для qr кода
     */
    public function confirm_qr()
    {
        if (!empty($_POST) && $this->session->data['payment_method']['code'] == 'safepay') {

            $error = array();

            $token = $this->esc_html($_POST['token']);
            if (!preg_match("~^[a-zA-Z0-9]*$~", $token) || $token !== $this->token) {
                $error['data'][] = $this->language->get('sp_valid_token');
            }

            $order_num = $this->esc_html($_POST['order_num']);
            if (!filter_var($order_num, FILTER_VALIDATE_INT)) {
                $error['data'][] = $this->language->get('sp_valid_order_id');
            }

            if (isset($error['data']) && count($error['data']) > 0) {
                $error['status'] = 'error';

                die(json_encode($error));
            }

            $res = array(
                'status' => 'OK',
                'data'   => $order_num
            );

            $this->load->model('checkout/order');

            if ($this->model_checkout_order->getOrder($order_num)['order_status_id'] == 0) {
                $this->model_checkout_order->addOrderHistory($order_num, 1);
            }

            $this->cart->clear();

            $json = json_encode($res);
        } else {
            $error['status'] = 'error';
            $json = json_encode($error);
        }

        die($json);
    }

    /**
     * Страница проверки оплаты
     *
     * @param int $order_id
     */
    public function success($order_id = 0)
    {
        if ($this->request->get['order_id']) {
            $order_id = $this->request->get['order_id'];
        }

        if (!isset($this->request->get['hash']) || !$hash_get = $this->request->get['hash']) {
            $hash_get = false;
        }

        $this->load->model('checkout/order');
        $order = $this->model_checkout_order->getOrder($order_id);

        $this->document->setTitle($this->language->get('text_title'));

        $data = array();
        $data += $this->success_head();

        $hash = md5(md5($order['order_id'] . '/' . $order['email'] . '/' . $order['telephone'] . '/' . $order['date_added']));

        if (!$order || $hash != $hash_get) {
            $data['sp_type_success'] = 'false';
        } elseif ($invoice = DBInvoice::getInvoiceByID($order_id, 'PAY_NUM', 'active', true)) {
            $recipient = DBRecipient::getByAttribute($invoice->RECIPIENT);
            $data['sp_check_pay'] = sprintf($this->language->get('sp_check_pay'), '<b>' . $recipient->NAME . '</b>');
            $data['sp_get_signature'] = $invoice->BANK_ID;
            $data['sp_get_expire'] = $invoice->EXPIRE - time();
            $data['sp_get_order_id'] = $order_id;
            $data['sp_get_amount'] = round($order['total'], 2) . ' ' . $order['currency_code'];
            $data['code_js'] = $this->getCodeCheck();
            $data['sp_type_success'] = 'check';
        } elseif ($invoice = DBInvoice::getInvoiceByID($order_id, 'PAY_NUM', 'finish', true)) {
            $data['sp_type_success'] = 'finish';
        } elseif ($invoice = DBInvoice::getInvoiceByID($order_id, 'PAY_NUM', 'expired',
                true) || $order['order_status_id'] == 1) {
            $data += $this->getSendOrderData($order);
            $data['code_js'] = $this->getCodeSend($order);
            $data['sp_type_success'] = 'send';
            $data['sp_get_order_id'] = $order_id;
            $data['sp_get_amount'] = round($order['total'], 2) . ' ' . $order['currency_code'];
            $this->session->data['payment_method']['code'] = 'safepay';
        } else {
            $data['sp_type_success'] = 'false';
        }

        $this->response->setOutput($this->load->view('extension/payment/safepay_success', $data));
    }

    /**
     * Страница проверки оплаты
     *
     * @param int $order_id
     */
    public function success_qr($order_id = 0)
    {
        if ($this->request->get['order_id']) {
            $order_id = $this->request->get['order_id'];
        }

        if (!isset($this->request->get['hash']) || !$hash_get = $this->request->get['hash']) {
            $hash_get = false;
        }

        $this->document->setTitle($this->language->get('text_title'));

        $data = array();
        $data += $this->success_head();

        $this->load->model('checkout/order');
        $order = $this->model_checkout_order->getOrder($order_id);

        $qr = $this->loadQR($order);

        $hash = md5(md5($order['order_id'] . '/' . $order['email'] . '/' . $order['telephone'] . '/' . $order['date_added']));

        if ($qr && $order_id && $order && $hash === $hash_get) {
            $data['sp_type_success'] = 'true';
            $data['sp_qr'] = $qr;
            $data['sp_url_bank'] = $this->url->link('extension/payment/safepay/success&order_id=' . $order_id . '&hash=' . $hash);
        } else {
            $data['sp_type_success'] = 'false';
        }

        $this->response->setOutput($this->load->view('extension/payment/safepay_success_qr', $data));
    }

    /**
     * Данные по странице спасибо
     *
     * @return array
     */
    private function success_head()
    {
        $data = array();

        $data += $this->getBreadcrumbs();
        $data += $this->load->language('extension/payment/safepay');

        $data['button_continue'] = $this->language->get('button_continue');
        $data['continue'] = $this->url->link('common/home');

        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');

        $code_css = file_get_contents(Options::getSiteUrl() . '/catalog/view/theme/default/stylesheet/safepay/public.css?t=' . time());
        $data['code_css'] = $code_css;

        return $data;
    }

    /**
     * Проверка поступления оплаты
     */
    public function check()
    {
        $signature = $this->esc_html($_POST['signature']);
        if (!preg_match("~^[a-zA-Z0-9]*$~", $signature)) {
            die(json_encode(false));
        }
        $order_id = $this->esc_html($_POST['order_id']);
        if (!filter_var($order_id, FILTER_VALIDATE_INT)) {
            die(json_encode(false));
        }
        $process = new Process();
        $process->resultPay($signature);
        $json = json_encode(DBInvoice::getInvoiceByID($signature, 'BANK_ID', DBInvoice::STATUS_FINISH));
        die($json);
    }

    /**
     * Запуск проверки выполнения задач Cron
     */
    public function check_cron()
    {
        $this->cron_pay();
        $this->cron_server();
    }

    /**
     * Задача для проверки серверов
     */
    public function cron_server()
    {
        $last_update = $this->config->get('safepay_cron_server');
        $interval_server = $this->config->get('safepay_interval_server') * 60;
        $end_time = $last_update + $interval_server;

        if ($end_time < time() || date("d.m.Y.H.i", $end_time) == date("d.m.Y.H.i", time())) {
            $cron = new Cron();
            $cron->availableServers();
            $this->model_extension_payment_safepay->setParamModule('safepay_cron_server', time());
        }
    }

    /**
     * Задача для проверки оплат
     */
    public function cron_pay()
    {
        $last_update = $this->config->get('safepay_cron_pay');
        $interval_pay = $this->config->get('safepay_interval_pay') * 60;
        $end_time = $last_update + $interval_pay;

        if ($end_time < time() || date("d.m.Y.H.i", $end_time) == date("d.m.Y.H.i", time())) {
            $cron = new Cron();
            $cron->resultPay();
            $this->model_extension_payment_safepay->setParamModule('safepay_cron_pay', time());
        }
    }

    /**
     * Обработка callback-ссылки и запуск проверки оплат
     */
    public function callback()
    {
        $request = array();

        if (empty($_POST['sign']) || empty($_POST['redirect'])) {
            die(json_encode(false));
        }

        $request['sign'] = $this->esc_html($_POST['sign']);
        if (!preg_match("~^[a-zA-Z0-9]*$~", $request['sign'])) {
            die(json_encode(false));
        }

        $request['redirect'] = $this->esc_html($_POST['redirect']);

        $process = new Process();
        $result = $process->resultPay($request['sign']);
        if ($request['redirect'] && $result) {
            die(json_encode(true));
        } else {
            die(json_encode(false));
        }
    }

    /**
     * Обработка js-скрипта для отправки инвойса
     *
     * @param array $order
     * @return string
     */
    private function getCodeSend($order)
    {
        if (!empty($this->config->get('safepay_qr_status')) && !empty($this->config->get('safepay_qr_name')) && !empty($this->config->get('safepay_qr_number'))) {
            $qr = '<a class="sp-popup__button sp-popup__button--qr" href="javascript:">' . $this->language->get('sp_link_qr_text') . '</a>';
        } else {
            $qr = '';
        }

        if (!empty($order)) {

            if (!$order_id = $order['order_id']) {
                $order_id = 0;
            }

            $hash = md5(md5($order['order_id'] . '/' . $order['email'] . '/' . $order['telephone'] . '/' . $order['date_added']));

        } else {
            $order_id = 0;
            $hash = 0;
        }

        if (empty($this->getPhone($order['telephone'])) || strlen($this->getPhone($order['telephone'])) < 10) {
            $check_phone = '';
            $check_phone .= '<div class="sp-popup__phone">';
            $check_phone .= '<p>'.$this->language->get('sp_send_no_phone').'</p>';
            $check_phone .= '<input type="tel" name="new_phone" placeholder="'.$this->language->get('sp_send_phone_text').'">';
            $check_phone .= '</div>';
        } else {
            $check_phone = '';
        }

        $code_js = file_get_contents(Options::getSiteUrl() . '/catalog/view/javascript/safepay/public.js?t=' . time());
        $code_js = str_replace('%SP_CHANGE_BANK%', $this->language->get('sp_change_bank'), $code_js);
        $code_js = str_replace('%SP_SEND_ORDER%', $this->language->get('sp_send_order'), $code_js);
        $code_js = str_replace('%SP_SEND_ERROR%', $this->language->get('sp_send_error'), $code_js);
        $code_js = str_replace('%SP_VOTE%',
            sprintf($this->language->get('sp_vote'), '<a href="https://safe-pay.ru/?page_id=4720" target="_blank">',
                '</a>'), $code_js);
        $code_js = str_replace('%SP_LINK_BANK%', $this->language->get('sp_link_bank'), $code_js);
        $code_js = str_replace('%SP_INSTR%',
            sprintf($this->language->get('sp_instr'),
                '<a href="https://safe-pay.ru" target="_blank">safe-pay.ru</a>'),
            $code_js);
        $code_js = str_replace('%SP_REDIRECT%',
            $this->url->link('extension/payment/safepay/success&order_id=' . $order_id . '&hash=' . $hash), $code_js);
        $code_js = str_replace('%SP_TOKEN%', $this->token, $code_js);
        $code_js = str_replace('%SP_URL_QR%',
            $this->url->link('extension/payment/safepay/success_qr&order_id=' . $order_id . '&hash=' . $hash),
            $code_js);
        $code_js = str_replace('%SP_LINK_QR%', $qr, $code_js);
        $code_js = str_replace('%SP_CHECK_PHONE%', $check_phone, $code_js);

        return $code_js;
    }

    /**
     * Обработка js-скрипта для проверки платежей
     *
     * @return string
     */
    private function getCodeCheck()
    {
        $code_js = file_get_contents(Options::getSiteUrl() . '/catalog/view/javascript/safepay/public_success.js?t=' . time());
        $code_js = str_replace('%SP_CHECK_FALSE%',
            sprintf($this->language->get('sp_check_false'), Options::getPayInterval()), $code_js);
        $code_js = str_replace('%SP_CHECK_TRUE%', $this->language->get('sp_check_true'), $code_js);
        $code_js = str_replace('%SP_CHECK_TITLE%', $this->language->get('sp_check_title'), $code_js);
        $code_js = str_replace('%SP_CHECK_LINK%', $this->language->get('sp_check'), $code_js);
        $code_js = str_replace('%SP_CHECK_DESC%', $this->language->get('sp_check_desc'), $code_js);
        $code_js = str_replace('%SP_TOKEN%', $this->token, $code_js);

        return $code_js;
    }

    /**
     * Получение данных заказа для отправки инвойса
     *
     * @param array $order
     * @return array
     */
    private function getSendOrderData($order)
    {
        $data = array();

        $param = array(
            'order_id'   => $order['order_id'],
            'order_date' => $order['date_added'],
            'order_sum'  => $this->getAmount($order['total']) . ' ' . $order['currency_code'],
            'site_url'   => $_SERVER['SERVER_NAME'],
        );

        $date = date_parse_from_format('Y-m-d H:i:s', $order['date_added']);
        $timestamp = mktime($date['hour'], $date['minute'], $date['second'], $date['month'], $date['day'],
            $date['year']);
        $args = array(
            'order_date'    => $timestamp,
            'order_num'     => $order['order_id'],
            'userPhone'     => $this->getPhone($order['telephone']),
            'curr'          => $this->getCurrency($order['currency_code']),
            'sum'           => $this->getAmount($order['total']),
            'expire'        => time() + Options::getExpire() * 3600,
            'title'         => Options::getTemplateTitle($param),
            'description'   => Options::getTemplateDescription($param) . $this->checkTaxNDS(),
            'ALL_RECIPIENT' => json_encode(DBRecipient::getListAll())
        );
        $data['hide_param'] = $args;

        return $data;
    }

    /**
     * Чистка входящих данных
     *
     * @param string $text
     * @return string
     */
    private function esc_html($text)
    {
        $text = trim($text);
        $text = strip_tags($text);
        $text = htmlspecialchars($text, ENT_QUOTES);

        return $text;
    }

    /**
     * Конвертация валюты в ISO 4217 number-3
     *
     * @param string $currency валюта в ISO 4217 alfa-3
     *
     * @return int
     */
    private function getCurrency($currency)
    {
        $iso4217 = new ISO4217();
        $number_cur = $iso4217->getByAlpha3($currency);

        return (int)$number_cur['numeric'];
    }

    /**
     * Получение суммы в корректном виде
     *
     * @param string $amount
     *
     * @return string
     */
    private function getAmount($amount)
    {
        return number_format($amount, 2, '.', '');
    }

    /**
     * Получение телефона в корректном виде
     *
     * @param string $phone номер телефона
     *
     * @return string
     */
    private function getPhone($phone)
    {

        $phone = preg_replace('~\D+~', '', $phone);

        return substr($phone, -10);
    }

    /**
     * Создание сессии с csrf-токеном
     * @param bool $replace
     * @return string
     */
    private function genCsrf($replace = false)
    {
        if ($replace || empty($this->session->data['sp_csrf'])) {
            $this->session->data['sp_csrf'] = $this->random_strings(36);
        }
        return $this->session->data['sp_csrf'];
    }

    /**
     * Генерация хэша
     *
     * @param int $length_of_string
     * @return string
     */
    private function random_strings($length_of_string)
    {
        $str_result = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';

        return substr(str_shuffle($str_result), 0, $length_of_string);
    }

    /**
     * Получения статуса с НДС или без
     *
     * @return string
     */
    private function checkTaxNDS()
    {
        $this->load->model('account/order');

        $products = $this->model_account_order->getOrderProducts($this->session->data['order_id']);
        $all = $this->getAllTax($this->getOrderItems($products));

        foreach ($all as $item) {
            if (stripos($item['name'], 'НДС') !== false) {
                return ' (Включая НДС ' . round($item['rate']) . '%)';
            }
        }
        return ' (Без НДС)';
    }

    /**
     * Получение всех налогов для товаров
     *
     * @param array $order_items
     * @return array
     */
    private function getAllTax($order_items)
    {
        $array = array();
        foreach ($order_items as $item) {
            $tax_rates = $this->tax->getRates(100, $item);
            foreach ($tax_rates as $tax_rate) {
                if ($tax_rate['type'] == 'P') {
                    $array[] = array(
                        'name' => $tax_rate['name'],
                        'rate' => $tax_rate['rate']
                    );
                }
            }
        }
        return $array;
    }

    /**
     * Получение классов налогов всех товаров в корзине
     *
     * @param $cartProducts
     * @return array
     */
    private function getOrderItems($cartProducts)
    {
        if (empty($cartProducts)) {
            return array();
        }
        $this->load->model('catalog/product');
        $cartProducts = (array)$cartProducts;
        $items = array();
        foreach ($cartProducts as $cartProduct) {
            $productData = $this->model_catalog_product->getProduct($cartProduct['product_id']);
            $items[] = $productData['tax_class_id'];
        }

        return $items;
    }

    /**
     * Загрузка данных и формирование QR-кода
     *
     * @param $order
     * @return bool|string
     */
    private function loadQR($order)
    {
        if (!$this->config->get('safepay_qr_status')) {
            return false;
        }

        if (!$qr_name = $this->config->get('safepay_qr_name')) {
            return false;
        }

        if (!$qr_number = $this->config->get('safepay_qr_number')) {
            return false;
        }

        if ($this->config->get('safepay_qr_inn')) {
            $qr_inn = '|PayeeINN=' . $this->config->get('safepay_qr_inn');
        } else {
            $qr_inn = '';
        }

        if ($desc = $this->config->get('safepay_qr_title')) {
            $desc = str_replace('%order_id%', $order['order_id'], $desc);
            $desc = str_replace('%order_date%', $order['date_added'], $desc);
            $desc = str_replace('%order_sum%', $this->getAmount($order['total']) . ' ' . $order['currency_code'],
                $desc);
            $desc = str_replace('%site_url%', $_SERVER['SERVER_NAME'], $desc);
            $qr_title = '|Purpose=' . htmlspecialchars_decode($desc);
        } else {
            $qr_title = '';
        }

        $data = 'ST00012|Name=' . htmlspecialchars_decode($qr_name) . '|PersonalAcc=' . $qr_number . '|BankName=АКБ «Трансстройбанк» (АО)|BIC=044525326|CorrespAcc=30101810845250000326|Sum=' . round($order['total'] * 100) . $qr_inn . $qr_title . '|UIN=' . md5(time()) . '_' . $order['order_id'] . '|TechCode=15';
        if ($url_qr = $this->generateQR($data)) {
            return $url_qr;
        } else {
            return false;
        }
    }

    /**
     * Генерация QR-кода
     *
     * @param string $data
     * @return bool|string
     */
    private function generateQR($data)
    {
        $tempDir = DIR_IMAGE . 'temp/';

        if (!file_exists($tempDir)) {
            mkdir($tempDir);
        }

        $fileName = md5($data . time()) . '.png';

        $pngAbsoluteFilePath = $tempDir . $fileName;
        $urlRelativeFilePath = '/image/temp/' . $fileName;

        if (!file_exists($pngAbsoluteFilePath)) {
            QRcode::png($data, $pngAbsoluteFilePath);

            return $urlRelativeFilePath;
        } else {
            return false;
        }
    }

    /**
     * Получение хлебных крошек
     *
     * @return array
     */
    private function getBreadcrumbs()
    {
        $data = array();
        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        );
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('sp_cart'),
            'href' => $this->url->link('checkout/cart')
        );
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('sp_checkout'),
            'href' => $this->url->link('checkout/checkout', '', true)
        );
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('sp_success'),
            'href' => $this->url->link('checkout/success')
        );

        return $data;
    }
}