<?php

class SafePay
{
    function __construct()
    {

        $path = dirname(__FILE__);
        defined('SPSEC') || define('SPSEC', $path . '/safepay/');
        require_once SPSEC . 'autoload.php';
    }
}
