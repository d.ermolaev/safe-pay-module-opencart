<?php

namespace SafePay\Blockchain;

defined('SPSEC') || exit;

class Security
{
    /**
     * Проверка правильности токена
     *
     * @param string $token
     *
     * @return bool
     */
    public static function check_csrf($token)
    {
        if (!$_SESSION['csrf_sp'] || $token !== $_SESSION['csrf_sp']) {
            return false;
        }

        return true;
    }

    /**
     * Вывод токена в форму
     *
     * @param string $token
     *
     * @return string
     */
    public static function view_csrf($token)
    {
        return sprintf('<input id="csrf_sp" type="hidden" name="csrf_sp" value="%s" />', $token);
    }

    /**
     * Генерация токена
     *
     * @return string
     */
    public static function gen_csrf()
    {
        if (empty($_SESSION['csrf_sp'])) {
            $_SESSION['csrf_sp'] = substr(md5(openssl_random_pseudo_bytes(20)), -32);
        }

        return $_SESSION['csrf_sp'];
    }
}