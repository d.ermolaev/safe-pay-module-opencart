<?php

namespace SafePay\Blockchain;

use PDO;

defined('SPSEC') || exit;

class DBServerOpencart implements DBServerCMS
{

    /**
     * Получение списка серверов для API
     *
     * @param string $type
     * @param string $sort
     *
     * @return array|object|bool
     */
    public static function getServers($type, $sort)
    {
        try {
            $db = DB::getInstance();
            if ($sort == 'desc') {
                $query = "SELECT ID, TYPE_SERVER, URL_SERVER, TIME_UPDATE FROM " . DB_PREFIX . "safe_pay_server WHERE TYPE_SERVER = :sp_type ORDER BY TIME_UPDATE DESC";
            } else {
                $query = "SELECT ID, TYPE_SERVER, URL_SERVER, TIME_UPDATE FROM " . DB_PREFIX . "safe_pay_server WHERE TYPE_SERVER = :sp_type ORDER BY TIME_UPDATE ASC";
            }

            $sth = $db->prepare($query);
            $sth->execute(array(':sp_type' => $type));

            return $sth->fetchAll(PDO::FETCH_CLASS);
        } catch (Exception $e) {
            print $e->getMessage();
            return false;
        }
    }

    /**
     * Получаем все сервера
     *
     * @return array|object|bool
     */
    public static function getAllServers()
    {
        try {
            $db = DB::getInstance();
            $sth = $db->query("SELECT ID, TYPE_SERVER, URL_SERVER, TIME_UPDATE FROM " . DB_PREFIX . "safe_pay_server");

            return $sth->fetchAll(PDO::FETCH_CLASS);
        } catch (Exception $e) {
            print $e->getMessage();
            return false;
        }
    }

    /**
     * Обновление доступности серверов
     *
     * @param int $id
     * @param int $time
     *
     * @return bool
     */
    public static function updateServer($id, $time)
    {
        try {
            $db = DB::getInstance();
            $query = "UPDATE " . DB_PREFIX . "safe_pay_server SET TIME_UPDATE = :sp_time WHERE ID = :sp_id";
            $sth = $db->prepare($query);

            return $sth->execute(array(':sp_time' => $time, 'sp_id' => $id));
        } catch (Exception $e) {
            print $e->getMessage();
            return false;
        }
    }

    /**
     * Добавление нового сервера
     *
     * @param array $server_data
     *
     * @return bool|int
     */
    public static function addServer($server_data)
    {
        try {
            $db = DB::getInstance();
            $query = "INSERT INTO " . DB_PREFIX . "safe_pay_server (URL_SERVER, TYPE_SERVER) VALUES (:sp_url_server,:sp_type_server)";
            $sth = $db->prepare($query);

            if ($sth->execute(array(
                ':sp_url_server'  => $server_data['URL_SERVER'],
                ':sp_type_server' => $server_data['TYPE_SERVER']
            ))) {
                return $db->lastInsertId();
            } else {
                return false;
            }
        } catch (Exception $e) {
            print $e->getMessage();
            return false;
        }
    }

    /**
     * Получение сервера по url
     *
     * @param string $url
     *
     * @return object|bool
     */
    public static function getServerByURL($url)
    {
        try {
            $db = DB::getInstance();
            $query = "SELECT ID FROM " . DB_PREFIX . "safe_pay_server WHERE URL_SERVER = :sp_url_server";
            $sth = $db->prepare($query);
            $sth->execute(array(':sp_url_server' => $url));

            return $sth->fetch(PDO::FETCH_LAZY);
        } catch (Exception $e) {
            print $e->getMessage();
            return false;
        }
    }

    /**
     * Удаление сервера из списка
     *
     * @param int $id
     *
     * @return false|int
     */
    public static function deleteServer($id)
    {
        try {
            $db = DB::getInstance();
            $query = "DELETE FROM " . DB_PREFIX . "safe_pay_server WHERE ID = :sp_id";
            $sth = $db->prepare($query);

            return $sth->execute(array(':sp_id' => $id));
        } catch (Exception $e) {
            print $e->getMessage();
            return false;
        }
    }
}