<?php

namespace SafePay\Blockchain;

use PDO;

defined('SPSEC') || exit;

class DBRecipientOpencart implements DBRecipientCMS
{
    /**
     * Получение реципиента по аттрибуту
     *
     * @param string $attr
     *
     * @return object|bool
     */
    public static function getByAttribute($attr)
    {
        try {
            $db = DB::getInstance();
            $query = "SELECT NAME, ATTRIBUTE, BILD, PUBLIC_KEY, PAY_URL, APP_ANDROID, APP_IOS, PICTURE_URL FROM " . DB_PREFIX . "safe_pay_recipient WHERE ATTRIBUTE = :sp_attr";

            $sth = $db->prepare($query);
            $sth->execute(array(':sp_attr' => $attr));

            return $sth->fetch(PDO::FETCH_LAZY);
        } catch (Exception $e) {
            print $e->getMessage();
            return false;
        }
    }

    /**
     * Получение всех реципиентов
     * @return array|object|bool
     */
    public static function getListAll()
    {
        try {
            $db = DB::getInstance();
            $query = "SELECT NAME, ATTRIBUTE, BILD, PUBLIC_KEY, PAY_URL, APP_ANDROID, APP_IOS, PICTURE_URL FROM " . DB_PREFIX . "safe_pay_recipient";

            $sth = $db->query($query);
            return $sth->fetchAll(PDO::FETCH_CLASS);
        } catch (Exception $e) {
            print $e->getMessage();
            return false;
        }
    }
}