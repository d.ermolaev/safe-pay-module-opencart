<?php

namespace SafePay\Blockchain;

use PDO;

defined('SPSEC') || exit;

class DBInvoiceOpencart implements DBInvoiceCMS
{

    /**
     * Добавление инвойса в БД
     *
     * @param array $invoice
     *
     * @return int|bool
     */
    public static function addInvoice($invoice)
    {
        try {
            $db = DB::getInstance();
            $query = "INSERT INTO " . DB_PREFIX . "safe_pay_invoice (STATUS, RECIPIENT, BANK_ID, EXPIRE, PAY_NUM, IS_TEST, DATE_CREATED, CREATOR, SITE_URL) 
                        VALUES (:sp_status,:sp_recipient,:sp_bank_id,:sp_expire,:sp_pay_num,:sp_is_test,:sp_date_created,:sp_creator,:sp_site_url)";
            $sth = $db->prepare($query);

            if ($sth->execute(array(
                ':sp_status'       => $invoice['STATUS'],
                ':sp_recipient'    => $invoice['RECIPIENT'],
                ':sp_bank_id'      => $invoice['BANK_ID'],
                ':sp_expire'       => $invoice['EXPIRE'],
                ':sp_pay_num'      => $invoice['PAY_NUM'],
                ':sp_is_test'      => $invoice['IS_TEST'],
                ':sp_date_created' => $invoice['DATE_CREATED'],
                ':sp_creator'      => $invoice['CREATOR'],
                ':sp_site_url'     => $invoice['SITE_URL']
            ))) {
                return true;
            } else {
                return false;
            }
        } catch (Exception $e) {
            print $e->getMessage();
            return false;
        }
    }

    /**
     * Обновление статуса инвойса
     *
     * @param int $id
     * @param string $typeID
     * @param string $status
     *
     * @return bool
     */
    public static function updateStatus($id, $typeID, $status)
    {
        $security = array('ID', 'BANK_ID', 'PAY_NUM');
        if (!in_array($typeID, $security)) {
            return false;
        }
        try {
            $db = DB::getInstance();
            $query = "UPDATE " . DB_PREFIX . "safe_pay_invoice SET STATUS = :sp_status, DATE_UPDATED = :sp_time WHERE {$typeID} = :sp_id";
            $sth = $db->prepare($query);

            return $sth->execute(array(':sp_time' => time(), ':sp_status' => $status, ':sp_id' => $id));
        } catch (Exception $e) {
            print $e->getMessage();
            return false;
        }
    }

    /**
     * Проверка существования инвойса по ID заказа/инвойса и статусу
     *
     * @param int|string $ID
     * @param string $typeID
     * @param string $status
     * @param bool $getData
     *
     * @return bool|object
     */
    public static function getInvoiceByID($ID, $typeID, $status, $getData = false)
    {
        $security = array('ID', 'BANK_ID', 'PAY_NUM');
        if (!in_array($typeID, $security)) {
            return false;
        }
        try {
            $db = DB::getInstance();
            $query = "SELECT * FROM " . DB_PREFIX . "safe_pay_invoice WHERE STATUS = :sp_status AND {$typeID} = :sp_id";
            $sth = $db->prepare($query);
            $sth->execute(array(':sp_status' => $status, ':sp_id' => $ID));

            if ($invoice = $sth->fetch(PDO::FETCH_LAZY)) {
                if (!empty($getData)) {
                    return $invoice;
                }
                return true;
            } else {
                return false;
            }
        } catch (Exception $e) {
            print $e->getMessage();
            return false;
        }
    }

    /**
     * Деактивация устаревших инвойсов
     *
     * @return bool
     */
    public static function deactivateExpiring()
    {
        $activeInvoices = self::getAllInvoice(self::STATUS_ACTIVE);
        if ($activeInvoices) {
            foreach ($activeInvoices as $item) {
                if (time() > $item->EXPIRE) {
                    self::updateStatus($item->ID, 'ID', self::STATUS_EXPIRED);
                }
            }
        }

        return true;
    }

    /**
     * Получение инвойсов всех или отфильтрованных по статусу
     *
     * @param null|string $status
     *
     * @return array|object|bool
     */
    public static function getAllInvoice($status = null)
    {
        try {
            $db = DB::getInstance();
            if ($status === null) {
                $query = "SELECT ID, STATUS, RECIPIENT, BANK_ID, EXPIRE, PAY_NUM, IS_TEST, DATE_CREATED, DATE_UPDATED, CREATOR FROM " . DB_PREFIX . "safe_pay_invoice";
            } else {
                $query = "SELECT ID, EXPIRE, DATE_CREATED, PAY_NUM FROM " . DB_PREFIX . "safe_pay_invoice WHERE STATUS = :sp_status";
            }
            $sth = $db->prepare($query);
            $sth->execute(array(':sp_status' => $status));

            if ($invoice = $sth->fetchAll(PDO::FETCH_CLASS)) {
                return $invoice;
            } else {
                return false;
            }
        } catch (Exception $e) {
            print $e->getMessage();
            return false;
        }
    }
}