<?php

namespace SafePay\Blockchain;

defined('SPSEC') || exit;

class Request
{

    const STATUS_OK = 'OK';
    const STATUS_ERROR = 'ERROR';

    /**
     * Отправка запроса по API
     *
     * @param string $urlPrefix
     * @param array|bool $param
     * @param bool|null $test
     *
     * @return array|bool
     */
    public function send($urlPrefix, $param = false, $test = null)
    {
        foreach (self::getAvailable($test) as $server) {
            $url = $server->URL_SERVER . $urlPrefix;
            $response = self::makeCURl($url, $param);
            if ($response['STATUS'] == self::STATUS_OK) {
                return $response;
            }
        }

        return false;
    }

    /**
     * @param string $url
     * @param array|bool $param
     *
     * @return array
     */
    private static function makeCURl($url, $param = false)
    {
        $result = array();

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        if ($param) {
            curl_setopt($ch, CURLOPT_URL, $url . '?' . http_build_query($param));
        } else {
            curl_setopt($ch, CURLOPT_URL, $url);
        }
        $response = curl_exec($ch);

        if ($response === false) {
            $result['ERROR'] = curl_error($ch);
            $result['STATUS'] = self::STATUS_ERROR;
        } else {
            $result['DATA'] = $response;
            $result['STATUS'] = self::STATUS_OK;
        }
        curl_close($ch);

        return $result;
    }

    /**
     * Получение серверов для API
     *
     * @param bool|null $test
     *
     * @return array
     */
    private static function getAvailable($test = null)
    {
        $arServers = array();
        if ($test === null) {
            $test = Options::isTest();
        }
        if ($test) {
            $dbServer = DBServer::getServers('test', 'desc');
        } else {
            $dbServer = DBServer::getServers('live', 'desc');
        }

        foreach ($dbServer as $item) {
            $arServers[] = $item;
        }

        return $arServers;
    }

}