<?php

namespace SafePay\Blockchain;

defined('SPSEC') || exit;

class Options extends OptionsOpencart
{

    /**
     * Получение ссылки на сайт
     *
     * @return string
     */
    public static function getSiteUrl()
    {
        if (isset($_SERVER['HTTPS'])) {
            $protocol = ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off") ? "https" : "http";
        } else {
            $protocol = 'http';
        }

        return $protocol . "://" . $_SERVER['SERVER_NAME'];
    }

}