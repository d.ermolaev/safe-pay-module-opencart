(function($) {
  'use strict';
  $(document).ready(function() {
    let public_key = $('#sp_public_key').val();
    let private_key = $('#sp_private_key').val();
    let bild = $('#sp_bild').val();

    const ISSET_KEY = issetKey();

    function downloadTextFile(fileContent, fileName) {
      let fileLink = window.document.createElement('a');
      fileLink.href = window.URL.createObjectURL(
          new Blob([fileContent], {type: 'text/plain'}));
      fileLink.download = fileName;
      document.body.appendChild(fileLink);
      fileLink.click();
      document.body.removeChild(fileLink);
    }

    function issetKey() {
      if (public_key !== '' && public_key !== null && public_key !==
          undefined) return true;
      if (private_key !== '' && private_key !== null && private_key !==
          undefined) return true;
      if (bild !== '' && bild !== null && bild !== undefined) return true;

      return false;
    }

    function editKeysOn() {

      $('.sp-settings__message').slideDown('fast');
      $('.sp-settings__save').show();
      $('.sp-settings__button--download').hide();
    }

    function editKeysOff() {

      $('.sp-settings__message').slideUp('fast');
      $('.sp-settings__save').hide();
      $('.sp-settings__button--download').show();
    }

    function generateKey() {
      $.ajax({
        url: 'index.php?route=extension/payment/safepay/generate_key&token=%TOKEN%',
        type: 'POST',
        cache: false,
        beforeSend: function() {
          $('.sp-settings__loading').css({'display': 'inline-block'});
        },
        success: function(d) {
          $('.sp-settings__loading').css({'display': 'none'});

          let bild = getAccountAddressFromPublicKey(d.publicKey);
          $('#sp_public_key').val(d.publicKey);
          $('#sp_private_key').val(d.privateKey);
          $('#sp_bild').val(bild);
          $('#sp_last_block_test').val(d.lastBlockTest);
          $('#sp_last_block_live').val(d.lastBlock);

          editKeysOn();
        },
        error: function(xhr, ajaxOptions, thrownError) {
          console.log(thrownError + '\r\n' + xhr.statusText + '\r\n' +
              xhr.responseText);
        },
        dataType: 'json',
      });
    }

    if (ISSET_KEY === true) {
      $('.sp-settings__button--download').show();
    } else {
      $('.sp-settings__button--download').hide();
    }

    $('#sp_public_key, #sp_private_key, #sp_bild').
        on('input, keyup', function() {
          if ($('#sp_public_key').val() !== public_key
              || $('#sp_private_key').val() !== private_key
              || $('#sp_bild').val() !== bild
          ) {
            editKeysOn();
          } else {
            editKeysOff();
          }
        });

    $('.sp-settings__button--generate').click(function(e) {
      e.preventDefault();
      if (ISSET_KEY === true) {
        if (confirm('%WARNING_KEY%')) {
          generateKey();
        }
      } else {
        generateKey();
      }
    });

    $('.sp-settings__button--download').click(function(e) {
      e.preventDefault();
      let text = 'PublicKey: ' + public_key + '\r\n';
      text += 'PrivateKey: ' + private_key + '\r\n';
      text += 'Address: ' + bild;
      downloadTextFile(text, 'keys');
    });

    $('.sp-server__action--add').click(function(e) {
      e.preventDefault();
      let sp_form = $(this).parents('.sp-table__row--form');
      let data = {
        'URL_SERVER': sp_form.find('#URL_SERVER').val(),
        'TYPE_SERVER': sp_form.find('#TYPE_SERVER').val(),
      };
      $.ajax({
        url: 'index.php?route=extension/payment/safepay/add_server&token=%TOKEN%',
        method: 'post',
        data: data,
        success: function(data) {
          $('.sp-table__result').
              html('<div class="sp-table__result--' + data.status + '">' +
                  data.message + '</div>');
          if (data.status === 'success') {
            let html = '';
            html += '<tr class="sp-table__row" id="sp-table-server-' +
                data.server['id'] + '">';
            html += '<td>' + data.server['id'] + '</td>';
            html += '<td>' + data.server['url'] + '</td>';
            html += '<td>' + data.server['type'] + '</td>';
            html += '<td></td>';
            html += '<td>';
            html += '<a class="sp-server__action--del" href="#" data-server_id="' +
                data.server['id'] + '">%SP_SERVER_DEL%</a>';
            html += '</td>';
            html += '</tr>';
            $('.sp-table__row--form').before(html);
            $('#URL_SERVER').val('');
            $('#TYPE_SERVER').val('');
          }
        },
        error: function(xhr, ajaxOptions, thrownError) {
          console.log(thrownError + '\r\n' + xhr.statusText + '\r\n' +
              xhr.responseText);
        },
        dataType: 'json',
      });
    });

    $('body').on('click', '.sp-server__action--del', function(e) {
      e.preventDefault();
      let server_id = $(this).data('server_id');
      let data = {
        'server_id': server_id,
      };
      $.ajax({
        url: 'index.php?route=extension/payment/safepay/del_server&token=%TOKEN%',
        method: 'post',
        data: data,
        success: function(data) {
          $('.sp-table__result').
              html('<div class="sp-table__result--' + data.status + '">' +
                  data.message + '</div>');
          $('#sp-table-server-' + server_id).remove();
        },
        error: function(xhr, ajaxOptions, thrownError) {
          console.log(thrownError + '\r\n' + xhr.statusText + '\r\n' +
              xhr.responseText);
        },
        dataType: 'json',
      });
    });
  });
})(jQuery);