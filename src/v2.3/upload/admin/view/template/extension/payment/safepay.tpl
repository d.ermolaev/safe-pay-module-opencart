<?php /** @noinspection PhpUndefinedVariableInspection */
echo $header; ?>
<?php echo $column_left; ?>
    <div id="content">
        <div class="page-header">
            <div class="container-fluid">
                <div class="pull-right">
                    <button type="submit" form="form-payment" data-toggle="tooltip" title="<?php echo $button_save; ?>"
                            class="btn btn-primary"><i class="fa fa-save"></i></button>
                    <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>"
                       class="btn btn-default"><i
                                class="fa fa-reply"></i></a></div>
                <h1><i class="fa fa-credit-card"></i> <?php echo $heading_title; ?></h1>
                <ul class="breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb): ?>
                        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
        <div class="container-fluid">
            <?php if ($error_warning): ?>
                <div class="alert alert-danger alert-dismissible"><i
                            class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>
            <?php endif; ?>
            <div class="panel panel-default">
                <div class="panel-body sp-settings">
                    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-payment"
                          class="form-horizontal">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab_general"
                                                  data-toggle="tab"><?php echo $sp_tab_general; ?></a></li>
                            <li><a href="#tab_transactions" data-toggle="tab"><?php echo $sp_tab_transactions; ?></a>
                            </li>
                            <li><a href="#tab_extended" data-toggle="tab"><?php echo $sp_tab_extended; ?></a></li>
                            <li><a href="#tab_qr" data-toggle="tab"><?php echo $sp_tab_qr; ?></a></li>
                            <li><a href="#tab_server" data-toggle="tab"><?php echo $sp_tab_server; ?></a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_general">
                                <div class="form-group">
                                    <div class="sp_margin">
                                        <label class="col-sm-2 control-label"
                                               for="sp_payment_status"><?php echo $sp_payment_status; ?></label>
                                        <div class="col-sm-10">
                                            <select name="safepay_status" id="sp_payment_status"
                                                    class="form-control">
                                                <?php if ($safepay_status): ?>
                                                    <option value="1"
                                                            selected="selected"><?php echo $text_enabled; ?></option>
                                                    <option value="0"><?php echo $text_disabled; ?></option>
                                                <?php else: ?>
                                                    <option value="1"><?php echo $text_enabled; ?></option>
                                                    <option value="0"
                                                            selected="selected"><?php echo $text_disabled; ?></option>
                                                <?php endif; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="sp_margin">
                                        <label class="col-sm-2 control-label"
                                               for="safepay_geo"><?php echo $sp_payment_geo; ?></label>
                                        <div class="col-sm-10">
                                            <select name="safepay_geo" id="safepay_geo"
                                                    class="form-control">
                                                <option value="0"><?php echo $text_all_zones; ?></option>
                                                <?php foreach ($geoZones as $zone): ?>
                                                    <?php if ($zone['geo_zone_id'] == $safepay_geo): ?>
                                                        <option value="<?php echo $zone['geo_zone_id']; ?>"
                                                                selected="selected"><?php echo $zone['name']; ?></option>
                                                    <?php else: ?>
                                                        <option value="<?php echo $zone['geo_zone_id']; ?>"><?php echo $zone['name']; ?></option>
                                                    <?php endif; ?>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="sp_margin">
                                        <div class="col-sm-2"></div><!--empty-->
                                        <h4 class="col-sm-10"><?php echo $sp_general_h2_key; ?></h4>
                                    </div>
                                    <div class="sp_margin required">
                                        <label class="col-sm-2 control-label" for="sp_public_key">
                                            <?php echo $sp_general_public_key; ?>
                                        </label>
                                        <div class="col-sm-10">
                                            <input type="text" name="safepay_public_key"
                                                   value="<?php if (!empty($safepay_public_key)) {
                                                       echo $safepay_public_key;
                                                   } ?>" placeholder="<?php echo $sp_general_public_key; ?>"
                                                   id="sp_public_key" class="form-control"/>
                                        </div>
                                    </div>
                                    <div class="sp_margin required">
                                        <label class="col-sm-2 control-label" for="sp_private_key">
                                            <?php echo $sp_general_private_key; ?>
                                        </label>
                                        <div class="col-sm-10">
                                            <input type="text" name="safepay_private_key"
                                                   value="<?php if (!empty($safepay_private_key)) {
                                                       echo $safepay_private_key;
                                                   } ?>"
                                                   placeholder="<?php echo $sp_general_private_key; ?>"
                                                   id="sp_private_key"
                                                   class="form-control"/>
                                        </div>
                                    </div>
                                    <div class="sp_margin required">
                                        <label class="col-sm-2 control-label" for="sp_bild">
                                            <?php echo $sp_general_bild; ?>
                                        </label>
                                        <div class="col-sm-10">
                                            <input type="text" name="safepay_bild"
                                                   value="<?php if (!empty($safepay_bild)) {
                                                       echo $safepay_bild;
                                                   } ?>"
                                                   placeholder="<?php echo $sp_general_bild; ?>" id="sp_bild"
                                                   class="form-control"/>
                                        </div>
                                    </div>
                                    <div class="sp_margin">
                                        <div class="col-sm-2"></div><!--empty-->
                                        <div class="col-sm-10">
                                            <input type="hidden" name="safepay_last_t" id="sp_last_block_test"
                                                   value="<?php if (!empty($safepay_last_t)) {
                                                       echo $safepay_last_t;
                                                   } ?>">
                                            <input type="hidden" name="safepay_last_l" id="sp_last_block_live"
                                                   value="<?php if (!empty($safepay_last_l)) {
                                                       echo $safepay_last_l;
                                                   } ?>">
                                            <input type="hidden" name="safepay_cron_pay" id="sp_cron_pay"
                                                   value="<?php if (!empty($safepay_cron_pay)) {
                                                       echo $safepay_cron_pay;
                                                   } else {
                                                       echo time();
                                                   } ?>">
                                            <input type="hidden" name="safepay_cron_server" id="sp_cron_server"
                                                   value="<?php if (!empty($safepay_cron_server)) {
                                                       echo $safepay_cron_server;
                                                   } else {
                                                       echo time();
                                                   } ?>">
                                            <div class='sp-settings__message'><?php echo $sp_general_message; ?></div>
                                        </div>
                                    </div>
                                    <div class="sp_margin">
                                        <div class="col-sm-2"></div><!--empty-->
                                        <div class="col-sm-10">
                                            <a href="#"
                                               class="sp-settings__button sp-settings__button--generate"><?php echo $sp_general_generate; ?></a>
                                            <a href="#"
                                               class="sp-settings__button sp-settings__button--download"><?php echo $sp_general_download; ?></a>
                                            <div class="sp-settings__loading">
                                                <!--suppress HtmlUnknownTarget -->
                                                <img src="/admin/view/image/payment/safepay/loading_mini.gif"
                                                     alt="Loading">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="sp_margin">
                                        <div class="col-sm-2"></div><!--empty-->
                                        <h4 class="col-sm-10"><?php echo $sp_general_h2_dop; ?></h4>
                                    </div>
                                    <div class="sp_margin">
                                        <label class="col-sm-2 control-label" for="sp_test">
                                    <span data-toggle="tooltip"
                                          title="<?php echo $sp_general_test_desc; ?>"><?php echo $sp_general_test; ?></span>
                                        </label>
                                        <div class="col-sm-10">
                                            <?php if ($safepay_test): ?>
                                                <input type="checkbox" name="safepay_test" value="on"
                                                       id="sp_test"
                                                       class="form-control" checked/>
                                            <?php else: ?>
                                                <input type="checkbox" name="safepay_test" value="on"
                                                       id="sp_test"
                                                       class="form-control"/>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                    <div class="sp_margin required">
                                        <label class="col-sm-2 control-label" for="sp_interval_server">
                                    <span data-toggle="tooltip"
                                          title="<?php echo $sp_general_interval_server_desc; ?>"><?php echo $sp_general_interval_server; ?></span>
                                        </label>
                                        <div class="col-sm-10">
                                            <input type="text" name="safepay_interval_server"
                                                   value="<?php if (!empty($safepay_interval_server)) {
                                                       echo $safepay_interval_server;
                                                   } else {
                                                       echo $safepay_interval_server_default;
                                                   } ?>"
                                                   placeholder="<?php echo $sp_general_interval_server; ?> (<?php echo $sp_general_interval_min; ?>)"
                                                   id="sp_interval_server"
                                                   class="form-control"/>
                                        </div>
                                    </div>
                                    <div class="sp_margin required">
                                        <label class="col-sm-2 control-label" for="sp_interval_pay">
                                    <span data-toggle="tooltip"
                                          title="<?php echo $sp_general_interval_pay_desc; ?>"><?php echo $sp_general_interval_pay; ?></span>
                                        </label>
                                        <div class="col-sm-10">
                                            <input type="text" name="safepay_interval_pay"
                                                   value="<?php if (!empty($safepay_interval_pay)) {
                                                       echo $safepay_interval_pay;
                                                   } else {
                                                       echo $safepay_interval_pay_default;
                                                   } ?>"
                                                   placeholder="<?php echo $sp_general_interval_pay; ?> (<?php echo $sp_general_interval_min; ?>)"
                                                   id="sp_interval_pay"
                                                   class="form-control"/>
                                        </div>
                                    </div>
                                    <div class="sp_margin required">
                                        <label class="col-sm-2 control-label" for="sp_interval_pay_expired">
                                    <span data-toggle="tooltip"
                                          title="<?php echo $sp_general_interval_pay_expired_desc; ?>"><?php echo $sp_general_interval_pay_expired; ?></span>
                                        </label>
                                        <div class="col-sm-10">
                                            <input type="text" name="safepay_interval_pay_expired"
                                                   value="<?php if (!empty($safepay_interval_pay_expired)) {
                                                       echo $safepay_interval_pay_expired;
                                                   } else {
                                                       echo $safepay_interval_pay_e_default;
                                                   } ?>"
                                                   placeholder="<?php echo $sp_general_interval_pay_expired; ?> (<?php echo $sp_general_interval_hour; ?>)"
                                                   id="sp_interval_pay_expired"
                                                   class="form-control"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab_transactions">
                                <?php if ($sp_all_invoice): ?>
                                    <table class="sp-table">
                                        <tr>
                                            <th><?php echo $sp_trans_id; ?></th>
                                            <th><?php echo $sp_trans_date; ?></th>
                                            <th><?php echo $sp_trans_status; ?></th>
                                            <th><?php echo $sp_trans_sign; ?></th>
                                            <th><?php echo $sp_trans_sum; ?></th>
                                            <th><?php echo $sp_trans_more; ?></th>
                                        </tr>
                                        <?php foreach ($sp_all_invoice as $invoice): ?>
                                            <tr>
                                                <td><?php echo $invoice['ID']; ?></td>
                                                <td><?php echo $invoice['DATE_CREATED']; ?></td>
                                                <td>
                                                    <span class="sp-trans__status--<?php echo $invoice['STATUS']; ?>"><?php echo $sp_invoice_status[$invoice['STATUS']]; ?></span>
                                                </td>
                                                <td><?php echo $invoice['BANK_ID']; ?></td>
                                                <td><?php echo $invoice['AMOUNT']; ?><?php echo $invoice['CURRENCY']['alpha3']; ?></td>
                                                <td>
                                                    <!--suppress HtmlUnknownTarget -->
                                                    <a href="/admin/index.php?route=sale/order/info&token=<?php echo $token; ?>&order_id=<?php echo $invoice['PAY_NUM']; ?>"><?php echo $sp_trans_order; ?>
                                                        #<?php echo $invoice['PAY_NUM']; ?></a></td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </table>
                                    <!--suppress HtmlUnknownTarget -->
                                    <a href="/admin/index.php?route=extension/payment/safepay/update_transactions&token=<?php echo $token; ?>"
                                       class="sp-settings__button sp-settings__button--update"><?php echo $sp_status_update; ?></a>
                                <?php else: ?>
                                    <p><?php echo $sp_trans_no; ?></p>
                                <?php endif; ?>
                            </div>
                            <div class="tab-pane" id="tab_extended">
                                <div class="form-group row">
                                    <div class="sp_margin">
                                        <div class="col-sm-2"></div><!--empty-->
                                        <h4 class="col-sm-10"><?php echo $sp_extend_h2_template; ?></h4>
                                    </div>
                                    <div class="sp_margin required">
                                        <label class="col-sm-2 control-label" for="sp_extended_temp_title">
                                            <?php echo $sp_extend_temp_title; ?>
                                        </label>
                                        <div class="col-sm-10">
                                            <input type="text" name="safepay_temp_title"
                                                   value="<?php if (!empty($safepay_temp_title)) {
                                                       echo $safepay_temp_title;
                                                   } else {
                                                       echo $safepay_temp_title_default;
                                                   } ?>"
                                                   placeholder="<?php echo $sp_extend_temp_title; ?>"
                                                   id="sp_extended_temp_title"
                                                   class="form-control"/>
                                        </div>
                                    </div>
                                    <div class="sp_margin required">
                                        <label class="col-sm-2 control-label" for="sp_extended_temp_desc">
                                            <?php echo $sp_extend_temp_desc; ?>
                                        </label>
                                        <div class="col-sm-10">
                                            <input type="text" name="safepay_temp_desc"
                                                   value="<?php if (!empty($safepay_temp_desc)) {
                                                       echo $safepay_temp_desc;
                                                   } else {
                                                       echo $safepay_temp_desc_default;
                                                   } ?>"
                                                   placeholder="<?php echo $sp_extend_temp_desc; ?>"
                                                   id="sp_extended_temp_desc"
                                                   class="form-control"/>
                                        </div>
                                    </div>
                                    <div class="sp_margin">
                                        <label class="col-sm-2 control-label" for="sp_extended_temp_desc">
                                            <?php echo $sp_extend_temp_param; ?>
                                        </label>
                                        <div class="col-sm-10 sp-extend__param">
                                            <p><code>%order_id%</code> - <?php echo $sp_extend_temp_param_id; ?></p>
                                            <p><code>%order_date%</code> - <?php echo $sp_extend_temp_param_date; ?></p>
                                            <p><code>%order_sum%</code> - <?php echo $sp_extend_temp_param_sum; ?></p>
                                            <p><code>%site_url%</code> - <?php echo $sp_extend_temp_param_site; ?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab_qr">
                                <div class="form-group">
                                    <div class="sp_margin required">
                                        <label class="col-sm-2 control-label"
                                               for="sp_payment_qr_status"><?php echo $sp_payment_status; ?></label>
                                        <div class="col-sm-10">
                                            <select name="safepay_qr_status" id="sp_payment_qr_status"
                                                    class="form-control">
                                                <?php if ($safepay_qr_status): ?>
                                                    <option value="1"
                                                            selected="selected"><?php echo $text_enabled; ?></option>
                                                    <option value="0"><?php echo $text_disabled; ?></option>
                                                <?php else: ?>
                                                    <option value="1"><?php echo $text_enabled; ?></option>
                                                    <option value="0"
                                                            selected="selected"><?php echo $text_disabled; ?></option>
                                                <?php endif; ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="sp_margin">
                                        <div class="col-sm-2"></div><!--empty-->
                                        <h4 class="col-sm-10"><?php echo $sp_qr_h2_template; ?></h4>
                                    </div>
                                    <div class="sp_margin required">
                                        <label class="col-sm-2 control-label" for="sp_qr_name">
                                            <?php echo $sp_qr_name; ?>
                                        </label>
                                        <div class="col-sm-10">
                                            <input type="text" name="safepay_qr_name"
                                                   value="<?php if(!empty($safepay_qr_name)) { echo $safepay_qr_name; } ?>"
                                                   placeholder="<?php echo $sp_qr_name_place; ?>" id="sp_qr_name"
                                                   class="form-control"/>
                                        </div>
                                    </div>
                                    <div class="sp_margin required">
                                        <label class="col-sm-2 control-label" for="sp_qr_number">
                                            <?php echo $sp_qr_number; ?>
                                        </label>
                                        <div class="col-sm-10">
                                            <input type="text" name="safepay_qr_number"
                                                   value="<?php if(!empty($safepay_qr_number)) { echo $safepay_qr_number; } ?>"
                                                   placeholder="<?php echo $sp_qr_number_place; ?>" id="sp_qr_number"
                                                   class="form-control"/>
                                        </div>
                                    </div>
                                    <div class="sp_margin">
                                        <label class="col-sm-2 control-label" for="sp_qr_inn">
                                            <?php echo $sp_qr_inn; ?>
                                        </label>
                                        <div class="col-sm-10">
                                            <input type="text" name="safepay_qr_inn"
                                                   value="<?php if(!empty($safepay_qr_inn)) { echo $safepay_qr_inn; } ?>"
                                                   placeholder="<?php echo $sp_qr_inn_place; ?>" id="sp_qr_inn"
                                                   class="form-control"/>
                                        </div>
                                    </div>
                                    <div class="sp_margin">
                                        <label class="col-sm-2 control-label" for="sp_qr_title">
                                            <?php echo $sp_qr_title; ?>
                                        </label>
                                        <div class="col-sm-10">
                                            <input type="text" name="safepay_qr_title"
                                                   value="<?php if(!empty($safepay_qr_title)) { echo $safepay_qr_title; } else { echo $safepay_qr_title_default; }?>"
                                                   placeholder="<?php echo $sp_qr_title_place; ?>" id="sp_qr_title"
                                                   class="form-control" maxlength="210"/>
                                        </div>
                                    </div>
                                    <div class="sp_margin">
                                        <label class="col-sm-2 control-label">
                                            <?php echo $sp_extend_temp_param; ?>
                                        </label>
                                        <div class="col-sm-10 sp-extend__param">
                                            <p><code>%order_id%</code> - <?php echo $sp_extend_temp_param_id; ?></p>
                                            <p><code>%order_date%</code> - <?php echo $sp_extend_temp_param_date; ?></p>
                                            <p><code>%order_sum%</code> - <?php echo $sp_extend_temp_param_sum; ?></p>
                                            <p><code>%site_url%</code> - <?php echo $sp_extend_temp_param_site; ?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab_server">
                                <?php if ($sp_all_server): ?>
                                    <table class="sp-table">
                                        <tr class="sp-table__row">
                                            <th><?php echo $sp_server_id; ?></th>
                                            <th><?php echo $sp_server_url; ?></th>
                                            <th><?php echo $sp_server_type; ?></th>
                                            <th><?php echo $sp_server_time; ?></th>
                                            <th><?php echo $sp_server_action; ?></th>
                                        </tr>
                                        <?php foreach ($sp_all_server as $server): ?>
                                            <tr class="sp-table__row" id="sp-table-server-<?php echo $server->ID; ?>">
                                                <td><?php echo $server->ID; ?></td>
                                                <td><?php echo $server->URL_SERVER; ?></td>
                                                <td><?php echo $server->TYPE_SERVER; ?></td>
                                                <td><?php echo date("d.m.Y H:i",
                                                        round($server->TIME_UPDATE / 1000)); ?></td>
                                                <td>
                                                    <a href="#" class="sp-server__action--del"
                                                       data-server_id="<?php echo $server->ID; ?>"><?php echo $sp_server_del; ?></a>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>

                                        <tr class="sp-table__row sp-table__row--form">
                                            <td></td>
                                            <td>
                                                <input type="text" id="URL_SERVER"
                                                       placeholder="<?php echo $sp_server_url_desc; ?>">
                                            </td>
                                            <td>
                                                <input type="text" id="TYPE_SERVER"
                                                       placeholder="<?php echo $sp_server_type_desc; ?>">
                                            </td>
                                            <td>
                                                <input type="text" disabled=""
                                                       placeholder="<?php echo $sp_server_time_desc; ?>">
                                            </td>
                                            <td>
                                                <a href="#"
                                                   class="sp-settings__button sp-server__action--add"><?php echo $sp_server_add; ?></a>
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="sp-table__result"></div>
                                <?php else: ?>
                                    <p><?php echo $sp_server_no; ?></p>
                                <?php endif; ?>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <link rel="stylesheet" type="text/css" href="<?php echo $link_css; ?>">
    <script type="text/javascript" src="<?php echo $link_js_base58; ?>"></script>
    <script type="text/javascript" src="<?php echo $link_js_sha256; ?>"></script>
    <script type="text/javascript" src="<?php echo $link_js_ripemd160; ?>"></script>
    <script type="text/javascript" src="<?php echo $link_js_eralib; ?>"></script>
    <script type="text/javascript">
        <?php echo $code_js; ?>
    </script>
<?php echo $footer; ?>